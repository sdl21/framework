#!/bin/bash

# Couleurs pour le texte
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # Pas de couleur

echo -e "${GREEN}Mise à jour de la liste des paquets...${NC}"
sudo apt update

# Fonction pour installer une bibliothèque si elle n'est pas présente
install_package() {
    PACKAGE=$1
    LIB_NAME=$2
    if dpkg -l | grep -q "^ii  $PACKAGE"; then
        echo -e "${GREEN}$LIB_NAME est déjà installé.${NC}"
    else
        echo -e "${GREEN}Installation de $LIB_NAME...${NC}"
        sudo apt install -y $PACKAGE
        if [ $? -eq 0 ]; then
            echo -e "${GREEN}$LIB_NAME installé avec succès.${NC}"
        else
            echo -e "${RED}Échec de l'installation de $LIB_NAME.${NC}"
            exit 1
        fi
    fi
}

# Installer SDL2, SDL2_ttf, et SDL2_image
install_package libsdl2-2.0-0 "SDL2 Runtime"
install_package libsdl2-dev "SDL2 Development Files"
install_package libsdl2-ttf-2.0-0 "SDL2_ttf Runtime"
install_package libsdl2-ttf-dev "SDL2_ttf Development Files"
install_package libsdl2-image-2.0-0 "SDL2_image Runtime"
install_package libsdl2-image-dev "SDL2_image Development Files"

echo -e "${GREEN}Toutes les bibliothèques SDL2 nécessaires ont été installées avec succès !${NC}"


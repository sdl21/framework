sudo apt-get install libasound2-dev

git clone -b release-2.30.x https://github.com/libsdl-org/SDL.git
cd SDL
./configure
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr
make
sudo make install
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local
make
sudo make install

cd ../..

sudo apt install libfreetype6 libfreetype6-dev
git clone -b release-2.22.x https://github.com/libsdl-org/SDL_ttf.git
cd SDL_ttf
./configure
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr
make
sudo make install
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local
make
sudo make install

cd ../..


make
sudo make install

git clone -b release-2.8.x https://github.com/libsdl-org/SDL_image.git
cd SDL_image
./configure
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr
make
sudo make install
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local
make
sudo make install

cd ../..

sudo apt install libopusfile-dev libopus-dev
sudo apt install libxmp-dev
sudo apt install libfluidsynth-dev
sudo apt install libwavpack-dev
sudo apt install --reinstall alsa-utils pulseaudio

git clone -b release-2.8.x https://github.com/libsdl-org/SDL_mixer.git
cd SDL_mixer
./configure
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr
make
sudo make install
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local
make
sudo make install

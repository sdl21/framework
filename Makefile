CC := gcc
CFLAGS := -Wall -Wextra -pedantic -g $(shell sdl2-config --cflags) $(shell sdl2-config --cflags)
LDFLAGS := $(shell sdl2-config --libs) -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lm
BUILD_DIR := build
SRC_DIR := src
TARGET := game
DOXYGEN_CONFIG := Doxyfile
SRCS := $(wildcard $(SRC_DIR)/*.c)
OBJS := $(patsubst $(SRC_DIR)/%.c,$(BUILD_DIR)/%.o,$(SRCS))
DEPS := $(OBJS:.o=.d)

.PHONY: all clean

all: $(BUILD_DIR) $(TARGET)

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -MMD -c $< -o $@

$(TARGET): $(OBJS)
	$(CC) $^ $(LDFLAGS) -o $@

doxygen:
	doxygen $(DOXYGEN_CONFIG)

-include $(DEPS)

clean:
	rm -rf $(BUILD_DIR) $(TARGET)

/**
 * \brief contain all code implementation to manage animation of texture
 *
 * \file BLV_animation.c
 * \author Gautier Levesque
 * \date 04-08-2024
 */

#include "BLV_animation.h"

SDL_bool BLV_create_BLV_animation(BLV_Animation *animation, uint16_t animationSpeed, uint16_t animWidth, uint16_t animHeight, uint16_t offset_x, uint16_t offset_y, uint8_t maxFrameWidth, uint8_t maxFrameHeight)
{
    // SDL_Log("Starting to create an animation");
    if (animation->texture == NULL)
    {
        SDL_Log("Warning animation does not contain texture == NULL");
    }
    //memset(animation, 0, sizeof(BLV_Animation));

    if (maxFrameWidth == 0 || maxFrameHeight == 0)
    {
        SDL_Log("Warning animation is not set because maxFrame == 0");
    }
    animation->src = NULL;
    if (animWidth > 0 && animHeight > 0)
    {
        animation->src = malloc(sizeof(SDL_Rect) * maxFrameWidth * maxFrameHeight);
        if (animation->src == NULL)
        {
            perror("Malloc failed :");
            SDL_Log("Failed to create BLV animation cause malloc failed");
            return SDL_FALSE;
        }
        for (uint8_t j = 0; j < maxFrameHeight; ++j)
        {
            for (uint8_t i = 0; i < maxFrameWidth; ++i)
            {
                animation->src[i].x = i * animWidth + offset_x * animWidth;
                animation->src[i].y = j * animHeight + offset_y * animHeight;
                animation->src[i].w = animWidth;
                animation->src[i].h = animHeight;
            }
        }
    }

    animation->maxFrame = maxFrameWidth;
    animation->currentFramePlayed = 0;
    animation->animationSpeed = animationSpeed;
    animation->flip = SDL_FLIP_NONE;
    animation->animationWay = FRONT_WAY;
    animation->animationType = IDDLE_ANIMATION;

    // SDL_Log("Animation create success");
    return SDL_TRUE;
}

SDL_bool BLV_create_array_of_BLV_animation(BLV_Animation *array, BLV_Renderer *renderer)
{
    if (BLV_get_texture("male_warrior.png", &array[MALE_WARRIOR_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("male_wizard.png", &array[MALE_WIZARD_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("male_thief.png", &array[MALE_THIEF_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("male_archer.png", &array[MALE_ARCHER_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("male_knight.png", &array[MALE_KNIGHT_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("banner_hanging.png", &array[BANNER_HANGING_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("button_hoover.png", &array[BUTTON_HOOVER_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("button_iddle.png", &array[BUTTON_IDDLE_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("House01_Blue.png", &array[HOUSE01_BLUE_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("House01_Red.png", &array[HOUSE01_RED_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("img_fond.jpg", &array[IMG_FOND_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("Marble3.jpg", &array[MARBLE3_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("panel_brown_dark.png", &array[PANEL_BROWN_DARK_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_get_texture("tilemap_packed.png", &array[TILEMAP_PACKED_ANIMATION].texture, renderer) == SDL_FALSE)
        return SDL_FALSE;

    if (BLV_create_BLV_animation(&array[MALE_WARRIOR_ANIMATION], 150, 512, 512, 0, 0, 4, 16) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[MALE_WIZARD_ANIMATION], 150, 512, 512, 0, 0, 4, 16) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[MALE_THIEF_ANIMATION], 150, 512, 512, 0, 0, 4, 16) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[MALE_ARCHER_ANIMATION], 150, 512, 512, 0, 0, 4, 16) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[MALE_KNIGHT_ANIMATION], 150, 512, 512, 0, 0, 4, 16) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[BANNER_HANGING_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[BUTTON_HOOVER_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[BUTTON_IDDLE_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[HOUSE01_BLUE_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[HOUSE01_RED_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[IMG_FOND_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[MARBLE3_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[PANEL_BROWN_DARK_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_BLV_animation(&array[TILEMAP_PACKED_ANIMATION], 0, 0, 0, 0, 0, 1, 1) == SDL_FALSE)
        return SDL_FALSE;

    return SDL_TRUE;
}

void BLV_update_BLV_animation(BLV_Animation *animation)
{
    if (animation == NULL)
        return;

    if (SDL_GetTicks64() - animation->lastTimeAnimationUpdate < animation->animationSpeed)
        return;

    animation->lastTimeAnimationUpdate = SDL_GetTicks64();
    animation->currentFramePlayed += 1;
    if (animation->currentFramePlayed >= animation->maxFrame)
        animation->currentFramePlayed = 0;
}

SDL_bool BLV_clean_BLV_animation(BLV_Animation *animation)
{
    if (animation == NULL)
        return SDL_TRUE;

    if (animation->src != NULL)
        free(animation->src);

    memset(animation, 0, sizeof(BLV_Animation));

    return SDL_TRUE;
}
/**
 * \brief army file contain all code implementation for manage army
 *
 * \file BLV_army.c
 * \author Gautier Levesque
 * \date 30-11-2024
 */

#include "BLV_army.h"

SDL_bool BLV_init_army(BLV_Army *army, BLV_Animation *animations, BLV_Map *m, BLV_Team_enum team, uint32_t pos_x, uint32_t pos_y)
{
    if (pos_x >= m->width)
        pos_x = m->width - 2;
    if (pos_y >= m->height)
        pos_y = m->height - 2;

    BLV_Army_soldier_class armyLeaderChoice = rand() % MAX_CLASS;
    army->animation = malloc(sizeof(BLV_Animation));
    if (army->animation == NULL)
        return SDL_FALSE;

    memcpy(&army->animation, &animations[MALE_WARRIOR_ANIMATION + armyLeaderChoice], sizeof(BLV_Animation));

    army->army_team = team;
    army->current_animation = ARMY_IDLE_ANIMATION;
    army->pos.x = pos_x;
    army->pos.y = pos_y;
    army->pos.w = ARMY_WIDTH;
    army->pos.h = ARMY_HEIGHT;
    army->is_active = SDL_TRUE;
    if (team == PLAYER_TEAM)
        m->map[army->pos.x + army->pos.y * m->width].playerArmy = army;
    else
        m->map[army->pos.x + army->pos.y * m->width].npcArmy = army;
    army->army_npc_count = rand() % 10 + 5;
    // BLV_init_npcs(&army->army, renderer, (rand() % 10) + 5, 1);
    for (uint16_t i = 0; i < army->army_npc_count; ++i)
    {
        BLV_Army_soldier_class class = rand() % MAX_CLASS;
        army->army[class] += 1;
        army->count_of_npc_alive += 1;
    }
    army->is_hoover = SDL_FALSE;
    army->move_remaining = BASE_MOVE;
    army->selected = SDL_FALSE;
    return SDL_TRUE;
}

SDL_bool BLV_update_army(BLV_Army_array armies, BLV_Army *army, BLV_Map *m, int offsetX, int offsetY, State_of_game *state, SDL_bool *generateArmyStat, BLV_Mouse mouse)
{
    BLV_update_BLV_animation(army->animation);

    if (mouse.right_click_pressed == SDL_TRUE && mouse.old_right_click_pressed == SDL_FALSE && army->army_team == PLAYER_TEAM && army->is_active == SDL_TRUE)
    {
        SDL_Point mousePos = {(mouse.x - offsetX) / TILE_WIDTH, (mouse.y - offsetY) / TILE_HEIGHT};
        SDL_bool mouseTouchArmy = mousePos.x == army->pos.x && mousePos.y == army->pos.y;
        if (mouseTouchArmy == SDL_TRUE && army->selected == SDL_FALSE)
        {
            army->selected = SDL_TRUE;
            BLV_reset_map_can_move(m);
            BLV_set_map_can_move(army->pos.x, army->pos.y, army->move_remaining, m);
        }
        else if (mouseTouchArmy == SDL_FALSE)
        {
            if (BLV_someone_is_selected(armies) == SDL_FALSE)
                BLV_reset_map_can_move(m);
            army->selected = SDL_FALSE;
        }
    }

    if (BLV_someone_is_selected(armies) == SDL_FALSE && mouse.left_click_pressed == SDL_TRUE && mouse.old_left_click_pressed == SDL_FALSE)
    {
        SDL_Point mousePos = {(mouse.x - offsetX) / TILE_WIDTH, (mouse.y - offsetY) / TILE_HEIGHT};
        SDL_bool mouseTouchArmy = mousePos.x == army->pos.x && mousePos.y == army->pos.y;
        if (mouseTouchArmy == SDL_TRUE && *state != SEE_ARMY_STATE)
        {
            *generateArmyStat = SDL_TRUE;
            *state = SEE_ARMY_STATE;
        }
    }
    else if (army->selected == SDL_TRUE && mouse.left_click_pressed == SDL_TRUE && mouse.old_left_click_pressed == SDL_FALSE && army->army_team == 0)
    {
        int destX = (mouse.x - offsetX) / TILE_WIDTH;
        int destY = (mouse.y - offsetY) / TILE_HEIGHT;
        // SDL_Log("Clique souris : %d,%d", mouse.x, mouse.y);
        // SDL_Log("DestX %d, destY : %d", destX, destY);
        if (destX < (int)m->width && destY < (int)m->height && destX >= 0 && destY >= 0)
        {
            if (m->map[destX + destY * (int)m->width].can_move == SDL_TRUE)
            {
                uint32_t idx = army->pos.x + army->pos.y * (int)m->width;
                m->map[idx].playerArmy = NULL;
                if (army->pos.x - destX < 0)
                    army->animation->animationWay = LEFT_WAY;
                else if (army->pos.x - destX > 0)
                    army->animation->animationWay = RIGHT_WAY;
                army->move_remaining -= BLV_manhattan_dist(army->pos.x, army->pos.y, destX, destY);
                army->pos.x = destX;
                army->pos.y = destY;
                idx = army->pos.x + army->pos.y * (int)m->width;
                // SDL_Log("deplacement army to : %d", idx);
                m->map[idx].playerArmy = &army->army;

                BLV_reset_map_can_move(m);
                BLV_set_map_can_move(army->pos.x, army->pos.y, army->move_remaining, m);
            }
        }
    }

    return SDL_TRUE;
}

SDL_bool BLV_is_someone_alive(BLV_Army army)
{
    return army.count_of_npc_alive > 0;
}

void BLV_move_army(BLV_Army *army, BLV_Map *m)
{
    for (uint8_t i = 0; i < army->move_remaining; ++i)
    {
        int incX = rand() % 2 == 1 ? -1 : 1;
        int incY = rand() % 2 == 1 ? -1 : 1;

        m->map[army->pos.x + army->pos.y * m->width].npcArmy = NULL;

        army->pos.x += incX;
        army->pos.y += incY;
        if (army->pos.x < 0)
            army->pos.x = 0;
        if (army->pos.y < 0)
            army->pos.y = 0;
        if (army->pos.x >= (int)m->width)
            army->pos.x = m->width - 1;
        if (army->pos.y >= (int)m->height)
            army->pos.y = m->height - 1;

        if (incX > 0)
            army->animation->animationWay = LEFT_WAY;
        else if (incX < 0)
            army->animation->animationWay = RIGHT_WAY;

        m->map[army->pos.x + army->pos.y * m->width].npcArmy = army;
    }
}

SDL_bool BLV_clean_army(BLV_Army *army)
{
    if (army->animation != NULL)
    {
        free(army->animation);
        army->animation = NULL;
    }
    return SDL_TRUE;
}

SDL_bool BLV_someone_is_selected(BLV_Army_array armies)
{
    for (uint8_t i = 0; i < MAX_ARMY; ++i)
    {
        if (armies.armies[i].is_active == SDL_TRUE && armies.armies[i].selected == SDL_TRUE)
            return SDL_TRUE;
    }
    return SDL_FALSE;
}

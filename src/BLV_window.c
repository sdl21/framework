/**
 * \brief file contain all code logic for launch and manage a SDL_Window with SDL2
 *
 * \file BLV_window.c
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#include <string.h>
#include <stdio.h>

#include "BLV_window.h"

SDL_bool BLV_create_BLV_window(const char *path, BLV_Window *w)
{
    SDL_Log("Try to create BLV_Window");

    if (w == NULL)
    {
        SDL_Log("Failed to create BLV_Window, cause w is NULL");
        return SDL_FALSE;
    }

    if (path == NULL)
    {
        memset(w, 0, sizeof(BLV_Window));
        SDL_Log("Failed to create BLV_Window, cause path is NULL");
        return SDL_FALSE;
    }

    FILE *file = fopen(path, "r");
    if (file == NULL)
    {
        SDL_Log("Failed to create BLV_Window, cause can't open file %s", path);
        perror("File open");
        return SDL_FALSE;
    }

    fscanf(file, "%s %d %d %d %d %u\r\n", w->title, &w->width, &w->height, &w->pos_x, &w->pos_y, &w->flags);

    w->window = SDL_CreateWindow(w->title, w->pos_x, w->pos_y, w->width, w->height, w->flags);
    if (w->window == NULL)
    {
        SDL_Log("Failed to create SDL_Window with error : %s", SDL_GetError());
        return SDL_FALSE;
    }

    SDL_Log("Create BLV_Window success");
    return SDL_TRUE;
}

SDL_bool BLV_clean_BLV_window(BLV_Window *w)
{
    SDL_Log("Try to clean a BLV_Window");

    if (w == NULL)
    {
        SDL_Log("Failed to clean a BLV_Window, cause w is NULL");
        return SDL_FALSE;
    }

    if (w->window != NULL)
    {
        SDL_DestroyWindow(w->window);
        w->window = NULL;
    }
    else
        SDL_Log("WARNING : Clean BLV_Window ok but SDL_Window is NULL");

    memset(w, 0, sizeof(BLV_Window));

    SDL_Log("Clean BLV_Window success");
    return SDL_TRUE;
}
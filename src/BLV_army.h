/**
 * \brief army file contain all code definition for manage army
 *
 * \file BLV_army.h
 * \author Gautier Levesque
 * \date 30-11-2024
 */

#ifndef __BLV_ARMY_H__
#define __BLV_ARMY_H__

#include "BLV_map.h"
#include "BLV_util.h"
#include "BLV_event.h"

#define MAX_ARMY 10

#define ARMY_WIDTH 48

#define BASE_MOVE 2

#define ARMY_HEIGHT 48

typedef enum
{
    WARRIOR,
    KNIGHT,
    THIEF,
    MARKSMAN,
    WIZARD,
    MAX_CLASS
}BLV_Army_soldier_class;

/**
 * \brief use to define all type of animation and count of current animation
 */
typedef enum
{
    ARMY_IDLE_ANIMATION = 0,
    ARMY_WALK_ANIMATION,
    ARMY_ATTACK_ANIMATION,
    ARMY_DIE_ANIMATION,
    ARMY_MAX_ANIMATION
} BLV_Army_animation_type_enum;

typedef struct
{  
    uint8_t army[MAX_CLASS];
    uint16_t army_npc_count;
    uint16_t count_of_npc_alive;
    BLV_Team_enum army_team;

    BLV_Animation *animation;
    int8_t current_animation;  
    SDL_Rect pos;
    SDL_bool is_active;
    SDL_bool is_hoover;
    SDL_bool selected;
    int8_t move_remaining;
    uint16_t count_of_npc[MAX_CLASS];
}BLV_Army;

/**
 * \brief structure use to define an array of army
 */
typedef struct
{
    uint16_t armies_count;   /**<armies_count> how many army currently live */
    BLV_Army armies[MAX_ARMY]; /**<armies> array of all armies */
} BLV_Army_array;

SDL_bool BLV_init_army(BLV_Army *army, BLV_Animation *animations, BLV_Map *m, BLV_Team_enum team, uint32_t pos_x, uint32_t pos_y);

SDL_bool BLV_update_army(BLV_Army_array armies, BLV_Army *army, BLV_Map *m, int offsetX, int offsetY, State_of_game *state, SDL_bool *generateArmyStat, BLV_Mouse mouse);

void BLV_move_army(BLV_Army *army, BLV_Map *m);

SDL_bool BLV_clean_army(BLV_Army *army);

SDL_bool BLV_someone_is_selected(BLV_Army_array armies);
SDL_bool BLV_is_someone_alive(BLV_Army army);

#endif
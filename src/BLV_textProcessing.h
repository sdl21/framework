/**
 * \brief contain all code logic for manage font and text
 *
 * \file BLV_textProcessing.h
 * \author Gautier Levesque
 * \date 09-09-2024
 */

#ifndef __BLV_TEXTPROCESSING_H__
#define __BLV_TEXTPROCESSING_H__

#include <SDL.h>
#include <SDL_ttf.h>

#include "BLV_graphic.h"

/**
 * \brief Max caractere for a font name
 */
#define MAX_CAR_FONT_NAME 255

/**
 * \brief Max font open at same time
 */
#define MAX_FONT 50

/**
 * \brief use to define a font
 */
typedef struct
{
    TTF_Font *font;  /**<font> font open */
    uint8_t size;    /**<size> size of each caractere of font */
    SDL_bool active; /**<active> if this font is use */
    char name[MAX_CAR_FONT_NAME];  /**<name> font name */
} BLV_Font;

/**
 * \brief use to define an array of font
 */
typedef struct
{
    BLV_Font fonts[MAX_FONT]; /**<fonts> array of font */
    uint8_t font_count;       /**<font_count> how many font open */
} BLV_Font_array;

/**
 * \brief use to create all font
 *
 * \param path core path where locate all font
 * \param fonts array of font filled with create font
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool BLV_create_all_font(const char *path, BLV_Font_array *fonts);

/**
 * \brief use to clean all font that are previously openned
 * 
 * \param fonts array of all openned font
 * 
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool BLV_clean_all_fonts(BLV_Font_array *fonts);

/**
 * \brief use to get a font
 * 
 * \param font_path name of font
 * \param font pointer to parameter who will be filled with good font
 * \param size size of font, if font is not at good size, then we will change font size
 * \param fonts array of fonts
 * 
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool BLV_get_font(const char *font_path, BLV_Font **font, uint8_t size, BLV_Font_array *fonts);

/**
 * \brief use to create a text
 * 
 * \param text text to print
 * \param texture pointer to a BLV_Texture who will be filled
 * \param font font use to create text
 * \param r renderer use to create text
 * \param color color of text
 * 
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool BLV_create_text(const char *text, BLV_Texture *texture, BLV_Font *font, BLV_Renderer *r, SDL_Color color);

/**
 * \brief use to clean a text
 * 
 * \param tx pointer to text to clean
 * 
 * \return SDL_TRUE if cleaning success, SDL_FALSE instead
 */
SDL_bool BLV_clean_text(BLV_Texture *txt);

#endif
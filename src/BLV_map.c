/**
 * \brief map file contain all code implementation for manage map
 *
 * \file BLV_map.c
 * \author Gautier Levesque
 * \date 30-11-2024
 */

#include "BLV_map.h"
#include <stdlib.h>

SDL_bool BLV_init_map(BLV_Map *m, BLV_Animation *animations, uint32_t width, uint32_t height)
{
    SDL_Log("Create map");
    m->height = height;
    m->width = width;

    m->map = malloc(sizeof(BLV_Tile) * width * height);
    if (m->map == NULL)
        return SDL_FALSE;

    for (uint32_t i = 0; i < height; ++i)
    {
        for (uint32_t j = 0; j < width; ++j)
        {
            uint32_t idx = i * width + j;
            m->map[idx].is_blocking = SDL_FALSE;

            int x = rand() % 3;
            if (x == 2)
                x = rand() % 3;
            memcpy(&m->map[idx].tile, &animations[TILEMAP_PACKED_ANIMATION], sizeof(BLV_Animation));
            m->map[idx].tile.currentFramePlayed = 0;
            m->map[idx].tile.maxFrame = 1;
            m->map[idx].tile.src = malloc(sizeof(SDL_Rect));
            if(m->map[idx].tile.src == NULL)
                return SDL_FALSE;
            m->map[idx].tile.src->x = x;
            m->map[idx].tile.src->y = 0;
            m->map[idx].tile.src->w = 16;
            m->map[idx].tile.src->h = 16;

            m->map[idx].pos.r.x = j * TILE_WIDTH;
            m->map[idx].pos.r.y = i * TILE_HEIGHT;
            m->map[idx].pos.r.w = TILE_WIDTH;
            m->map[idx].pos.r.h = TILE_HEIGHT;
            m->map[idx].pos.fill = SDL_FALSE;
            m->map[idx].npcArmy = NULL;
            m->map[idx].playerArmy = NULL;
            m->map[idx].building = NULL;
            SDL_Color color = {255, 255, 255, 255};
            m->map[idx].pos.color = color;
            m->map[idx].can_move = SDL_FALSE;
        }
    }
    SDL_Log("Create map success");
    return SDL_TRUE;
}

SDL_bool BLV_update_map(/*BLV_Map *m, uint32_t delta_time, State_of_game state*/)
{
    return SDL_TRUE;
}

SDL_bool BLV_clean_map(BLV_Map *m)
{
    if (m->map != NULL)
    {
        for (uint32_t i = 0; i < m->height; ++i)
        {
            for (uint32_t j = 0; j < m->width; ++j)
            {
                free(m->map[i * m->width + j].tile.src);
                m->map[i * m->width + j].tile.src = NULL;
            }
        }

        free(m->map);
        m->map = NULL;
    }

    m->height = 0;
    m->width = 0;
    return SDL_TRUE;
}

void BLV_set_map_can_move(int x, int y, int8_t move, BLV_Map *m)
{
    for (int yBis = y - move; yBis <= y + move; ++yBis)
    {
        if (yBis < 0 || yBis >= (int)m->height)
            continue;

        for (int xBis = x - move; xBis <= x + move; ++xBis)
        {
            if (xBis < 0 || xBis >= (int)m->width || (xBis == x && yBis == y))
                continue;

            if (BLV_manhattan_dist(x, y, xBis, yBis) <= move)
                m->map[xBis + yBis * m->width].can_move = SDL_TRUE;
        }
    }
}

void BLV_reset_map_can_move(BLV_Map *m)
{
    for (uint32_t i = 0; i < m->height * m->width; ++i)
    {
        m->map[i].can_move = SDL_FALSE;
    }
}
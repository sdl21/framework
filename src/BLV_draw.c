/**
 * \brief contain all code implementation to draw all thing on screen
 *
 * \file BLV_draw.c
 * \author Gautier Levesque
 * \date 03-02-2024
 */
#include "BLV_draw.h"

SDL_bool BLV_set_renderer_draw_color(SDL_Renderer *renderer, SDL_Color color)
{
    if (SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a) < 0)
    {
        SDL_Log("Failed to set color on renderer, cause : %s", SDL_GetError());
        return SDL_FALSE;
    }

    return SDL_TRUE;
}

SDL_bool BLV_draw_point(SDL_Renderer *renderer, BLV_Point point)
{
    if (renderer == NULL)
    {
        SDL_Log("Failed to draw SDL_Point on renderer, cause renderer is NULL");
        return SDL_FALSE;
    }

    if (BLV_set_renderer_draw_color(renderer, point.color) == SDL_FALSE)
    {
        SDL_Log("Failed to draw point on renderer, cause set color failed");
        return SDL_FALSE;
    }

    if (SDL_RenderDrawPoint(renderer, point.p.x, point.p.y) < 0)
    {
        SDL_Log("Failed to draw SDL_Point on renderer, cause : %s", SDL_GetError());
        return SDL_FALSE;
    }

    return SDL_TRUE;
}

SDL_bool BLV_draw_line(SDL_Renderer *renderer, BLV_Line line)
{
    if (renderer == NULL)
    {
        SDL_Log("Failed to draw line on renderer, cause renderer is NULL");
        return SDL_FALSE;
    }

    if (BLV_set_renderer_draw_color(renderer, line.color) == SDL_FALSE)
    {
        SDL_Log("Failed to draw line on renderer, cause set color failed");
        return SDL_FALSE;
    }

    if (SDL_RenderDrawLine(renderer, line.x1, line.y1, line.x2, line.y2) < 0)
    {
        SDL_Log("Failed to draw line on renderer, cause : %s", SDL_GetError());
        return SDL_FALSE;
    }

    return SDL_TRUE;
}

SDL_bool BLV_draw_rect(SDL_Renderer *renderer, BLV_Rect rect)
{
    if (renderer == NULL)
    {
        SDL_Log("Failed to draw SDL_Rect on renderer, cause renderer is NULL");
        return SDL_FALSE;
    }

    if (BLV_set_renderer_draw_color(renderer, rect.color) == SDL_FALSE)
    {
        SDL_Log("Failed to draw SDL_Rect on renderer, cause set color failed");
        return SDL_FALSE;
    }

    if (rect.fill == SDL_TRUE)
    {
        if (SDL_RenderFillRect(renderer, &rect.r) < 0)
        {
            SDL_Log("Failed to draw SDL_Rect on renderer, cause : %s", SDL_GetError());
            return SDL_FALSE;
        }
    }
    else
    {
        if (SDL_RenderDrawRect(renderer, &rect.r) < 0)
        {
            SDL_Log("Failed to draw SDL_Rect on renderer, cause : %s", SDL_GetError());
            return SDL_FALSE;
        }
    }

    return SDL_TRUE;
}

SDL_bool BLV_draw_texture(SDL_Renderer *renderer, SDL_Texture *texture, const SDL_Rect *src, const SDL_Rect *dst)
{
    if (renderer == NULL)
    {
        SDL_Log("Failed to draw SDL_Texture on renderer, cause renderer is NULL");
        return SDL_FALSE;
    }

    if (dst == NULL)
    {
        SDL_Log("Failed to draw SDL_Texture on renderer, cause dst is NULL");
        return SDL_FALSE;
    }

    if (texture == NULL)
    {
        SDL_Log("Failed to draw SDL_Texture on renderer, cause texture is NULL");
        return SDL_FALSE;
    }

    if (SDL_RenderCopy(renderer, texture, src, dst) < 0)
    {
        SDL_Log("Failed to draw SDL_Texture on renderer, cause : %s", SDL_GetError());
        return SDL_FALSE;
    }

    return SDL_TRUE;
}

SDL_bool BLV_draw_textureEx(SDL_Renderer *renderer, SDL_Texture *texture, const SDL_Rect *src, const SDL_Rect *dst, const double angle, const SDL_Point *center, const SDL_RendererFlip flip)
{
    if (renderer == NULL)
    {
        SDL_Log("Failed to draw SDL_Texture on renderer, cause renderer is NULL");
        return SDL_FALSE;
    }

    if (dst == NULL)
    {
        SDL_Log("Failed to draw SDL_Texture on renderer, cause dst is NULL");
        return SDL_FALSE;
    }

    if (texture == NULL)
    {
        SDL_Log("Failed to draw SDL_Texture on renderer, cause texture is NULL");
        return SDL_FALSE;
    }

    if (SDL_RenderCopyEx(renderer, texture, src, dst, angle, center, flip) < 0)
    {
        SDL_Log("Failed to draw SDL_Texture on renderer, cause : %s", SDL_GetError());
        return SDL_FALSE;
    }

    return SDL_TRUE;
}
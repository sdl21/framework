/**
 * \brief building file contain all code implementation for manage building
 *
 * \file BLV_building.c
 * \author Gautier Levesque
 * \date 14-12-2024
 */

#include "BLV_building.h"

SDL_bool BLV_init_building(BLV_Map *m, BLV_Animation *animations, BLV_Building *building, BLV_Type_building_enum type, BLV_Team_enum team, uint32_t pos_x, uint32_t pos_y)
{
    if (pos_x >= m->width)
        pos_x = m->width - 1;
    if (pos_y >= m->height)
        pos_y = m->height - 1;

    switch (type)
    {
    case TYPE_BUILDING_CASTLE:

        building->animations[0] = malloc(sizeof(BLV_Animation));
        if (building->animations[0] == NULL)
            return SDL_FALSE;

        if (team == PLAYER_TEAM)
        {
            memcpy(&building->animations[0], &animations[HOUSE01_BLUE_ANIMATION], sizeof(BLV_Animation));
        }
        else
        {
            memcpy(&building->animations[0], &animations[HOUSE01_RED_ANIMATION], sizeof(BLV_Animation));
        }
        break;
    default:
        SDL_Log("BUILDING UNKNOW");
    }

    building->team = team;
    building->current_animation = 0;
    building->pos.x = pos_x;
    building->pos.y = pos_y;
    building->pos.w = BUILDING_WIDTH;
    building->pos.h = BUILDING_HEIGHT;
    building->is_active = SDL_TRUE;
    m->map[building->pos.x + building->pos.y * m->width].building = building;

    memset(building->count_of_npc, 0, sizeof(uint16_t) * MAX_CLASS);
    for (uint16_t i = 0; i < 50; ++i)
    {
        BLV_Army_soldier_class class = rand() % MAX_CLASS;
        building->count_of_npc[class] += 1;
    }

    return SDL_TRUE;
}

SDL_bool BLV_update_building(BLV_Building *building, BLV_Map *map, BLV_Army_array armies, BLV_Mouse mouse, uint32_t offsetX, uint32_t offsetY, State_of_game *state)
{
    for (uint8_t i = 0; i < BUILDING_MAX_ANIMATION; ++i)
    {
        BLV_update_BLV_animation(building->animations[i]);
    }

    if (BLV_someone_is_selected(armies) == SDL_FALSE && mouse.left_click_pressed == SDL_TRUE && mouse.old_left_click_pressed == SDL_FALSE)
    {
        SDL_Point mousePos = {(mouse.x - offsetX) / TILE_WIDTH, (mouse.y - offsetY) / TILE_HEIGHT};
        SDL_bool mouseTouchBuilding = mousePos.x == building->pos.x && mousePos.y == building->pos.y;
        if (mouseTouchBuilding == SDL_TRUE && *state != SEE_CASTLE_STATE)
        {
            *state = SEE_CASTLE_STATE;
        }
    }
    else if (mouse.left_click_pressed == SDL_TRUE && mouse.old_left_click_pressed == SDL_FALSE)
    {
        SDL_Point mousePos = {(mouse.x - offsetX) / TILE_WIDTH, (mouse.y - offsetY) / TILE_HEIGHT};
        SDL_bool mouseTouchBuilding = mousePos.x == building->pos.x && mousePos.y == building->pos.y;
        if (mouseTouchBuilding == SDL_TRUE && *state != MANAGE_CASTLE_STATE)
        {
            map->map[mousePos.x + mousePos.y * map->width].can_move = SDL_TRUE;
            if (map->map[mousePos.x + mousePos.y * map->width].playerArmy != NULL)
            {
                *state = MANAGE_CASTLE_STATE;
            }
        }
    }

    return SDL_TRUE;
}

SDL_bool BLV_building_make_recruitment(BLV_Building_array *buildings)
{
    if (buildings == NULL)
        return SDL_FALSE;

    for (uint16_t i = 0; i < buildings->building_count; ++i)
    {
        for (uint16_t j = 0; j < 20; ++j)
        {
            BLV_Army_soldier_class class = rand() % MAX_CLASS;
            buildings->buildings[j].count_of_npc[class] += 1;
        }
    }

    return SDL_TRUE;
}

SDL_bool BLV_clean_building(BLV_Building_array *array)
{
    if (array->buildings != NULL)
    {
        for (uint16_t i; i < array->building_count; ++i)
        {
            for (uint8_t j = 0; j < BUILDING_MAX_ANIMATION; ++j)
            {
                if (array->buildings[i].animations[j] != NULL)
                {
                    free(array->buildings[i].animations[j]);
                    array->buildings[i].animations[j] = NULL;
                }
            }
        }

        free(array->buildings);
        array->buildings = NULL;
    }

    return SDL_TRUE;
}
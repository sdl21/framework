/**
 * \brief gui file contain all code definition for manage gui
 *
 * \file BLV_gui.h
 * \author Gautier Levesque
 * \date 08-12-2024
 */

#ifndef __BLV_GUI_H__
#define __BLV_GUI_H__

#include "BLV_util.h"
#include "BLV_geometry.h"
#include "BLV_textProcessing.h"
#include "BLV_graphic.h"
#include "BLV_map.h"
#include "BLV_army.h"
#include "BLV_window.h"
#include "BLV_building.h"

typedef enum
{
    END_TURN_BTN,
    START_GAME_BTN,
    QUIT_GAME_BTN,
    BACK_TO_MAIN_MENU_WIN_BTN,
    BACK_TO_MAIN_MENU_LOOSE_BTN,
    LEAVE_SEE_ARMY_BTN,
    GO_OPTION_MENU_MAP_BTN,
    RETURN_TO_MAP_FROM_OPTION_MENU_BTN,
    BACK_TO_MAIN_MENU_OPTION_BTN,
    LEAVE_SEE_CASTLE_BTN,
    LEAVE_MANAGE_CASTEL_BTN,
    ADD_WARRIOR_TO_CASTEL_BTN,
    ADD_KNIGHT_TO_CASTEL_BTN,
    ADD_ROGUE_TO_CASTEL_BTN,
    ADD_MARKSMAN_TO_CASTEL_BTN,
    ADD_WIZARD_TO_CASTEL_BTN,
    ADD_WARRIOR_TO_ARMY_BTN,
    ADD_KNIGHT_TO_ARMY_BTN,
    ADD_ROGUE_TO_ARMY_BTN,
    ADD_MARKSMAN_TO_ARMY_BTN,
    ADD_WIZARD_TO_ARMY_BTN,
} BLV_button_type_enum;

typedef struct
{
    BLV_Texture txt_iddle;
    BLV_Texture txt_hoover;
    BLV_Texture txt_pressed;
    BLV_Rect pos_iddle;
    BLV_Rect pos_hoover;
    BLV_Rect pos_pressed;
    State_of_game state_of_active;
    SDL_bool is_active;
} BLV_Label;

typedef struct
{
    BLV_Label text;

    BLV_Rect rectIddle;
    BLV_Rect rectHoover;
    BLV_Rect rectPressed;
    BLV_Animation *animationIddle;
    BLV_Animation *animationHoover;
    BLV_Animation *animationPressed;

    SDL_bool is_hoover;
    SDL_bool is_pressed;
    State_of_game state_of_active;
    SDL_bool is_active;
    BLV_button_type_enum type_button;
} BLV_Button;

typedef struct
{
    BLV_Animation *animation;
    BLV_Rect pos;
    SDL_bool active;
    State_of_game state;
}BLV_Panel_picture;

typedef struct
{
    uint8_t max_label;
    uint8_t label_count;
    BLV_Label *lst_label;

    uint8_t max_button;
    uint8_t button_count;
    BLV_Button *lst_btn;

    uint8_t max_panel_picture;
    uint8_t panel_picture_count;
    BLV_Panel_picture *lst_panel_picture;

    State_of_game state_of_active;
    SDL_bool is_active;
} BLV_Panel;

SDL_bool BLV_create_label(BLV_Renderer *renderer, BLV_Label *lbl, BLV_Font *f, const char *txt, uint32_t x, uint32_t y, uint32_t width, uint32_t height, State_of_game state, SDL_Color txt_color_iddle, SDL_Color txt_color_hoover, SDL_Color txt_color_pressed);
SDL_bool BLV_create_button(BLV_Renderer *renderer, BLV_Button *btn, BLV_Font *f, const char *txt, uint32_t x, uint32_t y, uint32_t width, uint32_t height, State_of_game state, BLV_button_type_enum type_button, SDL_Color txt_color_iddle, SDL_Color txt_color_hoover, SDL_Color txt_color_pressed, SDL_Color iddle_color, SDL_Color hoover_color, SDL_Color pressed_color, BLV_Animation *iddleAnimation, BLV_Animation *hooverAnimation, BLV_Animation *pressedAnimation);
SDL_bool BLV_create_panel_picture(BLV_Panel_picture *picture, BLV_Animation *animation, uint32_t x, uint32_t y, uint32_t width, uint32_t height, State_of_game state);
SDL_bool BLV_create_panel(BLV_Panel *panel, uint8_t button_count, uint8_t label_count, uint8_t texture_count, State_of_game state);
SDL_bool BLV_manage_gui(BLV_Panel *panel_list, BLV_Animation *animations, State_of_game *state, BLV_Map *m, BLV_Building_array *buildings, BLV_Renderer *renderer, BLV_Army_array *armies, uint32_t idx_manage_castel, BLV_Font_array *fonts, BLV_Mouse mouse);
SDL_bool BLV_create_all_panel(BLV_Renderer *renderer, BLV_Window window, BLV_Panel *panel_list, BLV_Font_array *fonts, BLV_Animation *animations);
SDL_bool BLV_generate_manage_castel_text(BLV_Renderer *renderer, BLV_Army *army, BLV_Building building, BLV_Font_array *fonts, BLV_Panel *panel);

SDL_bool BLV_clean_panel(BLV_Panel *panel);
SDL_bool BLV_clean_label(BLV_Label *lbl);
SDL_bool BLV_clean_button(BLV_Button *btn);

#endif
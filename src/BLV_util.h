/**
 * \brief file contain all code definition for all utilitary function that can be use everywhere
 *
 * \file BLV_util.h
 * \author Gautier Levesque
 * \date 08-09-2024
 */

#ifndef __BLV_UTIL_H__
#define __BLV_UTIL_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <sys/time.h>
#include <math.h>

typedef enum
{
    MAP_STATE,
    MAIN_MENU_STATE,
    WIN_STATE,
    LOOSE_STATE,
    BATTLEFIELD_STATE,
    SEE_ARMY_STATE,
    OPTION_MENU_MAP_STATE,
    SEE_CASTLE_STATE,
    MANAGE_CASTLE_STATE,
    MAX_STATE,
    QUIT_GAME,
} State_of_game;

typedef enum
{
    PLAYER_TEAM,
    ENNEMY_TEAM
}BLV_Team_enum;

/**
 * \brief use to get a formated date
 *
 * \param buffer use at end of function to know formated date
 * \param buf_size at call give it buffer size
 */
void BLV_get_formated_date(char *buffer, size_t buf_size);

int BLV_manhattan_dist(int x1, int y1, int x2, int y2);

/**
 * \brief use to calculate cartesian distance
 *
 * \param pos_x_A use to define x position of A
 * \param pos_y_A use to define y position of A
 * \param pos_x_B use to define x position of B
 * \param pos_y_B use to define y position of B
 *
 * \return cartesian_dist
 */
double BLV_cartesian_dist(uint32_t pos_x_A, uint32_t pos_y_A, uint32_t pos_x_B, uint32_t pos_y_B);

#endif
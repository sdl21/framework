/**
 * \brief contain all code definition to draw all thing on screen
 *
 * \file BLV_draw.h
 * \author Gautier Levesque
 * \date 03-02-2024
 */
#ifndef __BLV_DRAW_H__
#define __BLV_DRAW_H__

#include <SDL.h>

#include "BLV_geometry.h"

/**
 * \brief use to set a color on a SDL_Renderer
 * 
 * \param renderer renderer where to set SDL_Color 
 * \param color current color to set on SDL_Renderer 
 * 
 * \return SDL_TRUE if success, SDL_FALSE instead
*/
SDL_bool BLV_set_renderer_draw_color(SDL_Renderer *renderer, SDL_Color color);

/**
 * \brief use to draw a SDL_point with a color on a SDL_Renderer
 * 
 * \param renderer renderer where to draw SDL_Point
 * \param point current point to draw
 * 
 * \return SDL_TRUE if success, SDL_FALSE instead
*/
SDL_bool BLV_draw_point(SDL_Renderer *renderer, BLV_Point point);

/**
 * \brief use to draw a line with a color on a SDL_Renderer
 * 
 * \param renderer renderer where to draw line
 * \param line current line to draw
 * 
 * \return SDL_TRUE if success, SDL_FALSE instead
*/
SDL_bool BLV_draw_line(SDL_Renderer *renderer, BLV_Line line);

/**
 * \brief use to draw a SDL_rect with a color on a SDL_Renderer
 * 
 * \param renderer renderer where to draw SDL_rect
 * \param rect current rect to draw
 * 
 * \return SDL_TRUE if success, SDL_FALSE instead
*/
SDL_bool BLV_draw_rect(SDL_Renderer *renderer, BLV_Rect rect);

/**
 * \brief use to draw an SDL_Texture with rendercopy
 * 
 * \param renderer renderer where to draw SDL_Texture
 * \param texture current texture to draw
 * \param src portion on texture we need to draw, NULL if we draw entire texture
 * \param dst where we draw texture on screen
 * 
 * \return SDL_TRUE if draw is success, SDL_FALSE if not or if some important argument is missing
 */
SDL_bool BLV_draw_texture(SDL_Renderer *renderer, SDL_Texture *texture, const SDL_Rect *src, const SDL_Rect *dst);

/**
 * \brief use to draw an SDL_Texture with rendercopyEx
 * 
 * \param renderer renderer where to draw SDL_Texture
 * \param texture current texture to draw
 * \param src portion on texture we need to draw, NULL if we draw entire texture
 * \param dst where we draw texture on screen
 * \param angle angle of rotation of texture
 * \param center center of rotation of texture
 * \param flip if texture face to left or right
 * 
 * \return SDL_TRUE if draw is success, SDL_FALSE if not or if some important argument is missing
 */
SDL_bool BLV_draw_textureEx(SDL_Renderer *renderer, SDL_Texture *texture, const SDL_Rect *src, const SDL_Rect *dst, const double angle, const SDL_Point *center, const SDL_RendererFlip flip);

#endif
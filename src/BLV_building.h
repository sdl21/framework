/**
 * \brief building file contain all code definition for manage building
 *
 * \file BLV_building.h
 * \author Gautier Levesque
 * \date 14-12-2024
 */

#ifndef __BLV_BUILDING_H__
#define __BLV_BUILDING_H__

#include "BLV_util.h"
#include "BLV_army.h"
#include "BLV_map.h"

#define BUILDING_MAX_ANIMATION 1

#define BUILDING_WIDTH 48

#define BUILDING_HEIGHT 48

typedef enum
{
    TYPE_BUILDING_CASTLE,
    MAX_TYPE_BUILDING,
}BLV_Type_building_enum;

typedef struct 
{
    BLV_Animation *animations[BUILDING_MAX_ANIMATION];
    int8_t current_animation;  
    SDL_Rect pos;
    uint16_t count_of_npc[MAX_CLASS];
    BLV_Type_building_enum type;
    BLV_Team_enum team;
    SDL_bool is_active;
}BLV_Building;

typedef struct
{
    uint16_t max_building;
    uint16_t building_count;
    BLV_Building *buildings;
}BLV_Building_array;

SDL_bool BLV_init_building(BLV_Map *m, BLV_Animation *animations, BLV_Building *building, BLV_Type_building_enum type, BLV_Team_enum team, uint32_t pos_x, uint32_t pos_y);
SDL_bool BLV_update_building(BLV_Building *building, BLV_Map *map, BLV_Army_array armies, BLV_Mouse mouse, uint32_t offsetX, uint32_t offsetY, State_of_game *state);
SDL_bool BLV_building_make_recruitment(BLV_Building_array *buildings);
SDL_bool BLV_clean_building(BLV_Building_array *array);


#endif
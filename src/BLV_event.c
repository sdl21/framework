/**
 * \brief event file contain all code implementation to manage all event, mouse and keyboard include of SDL2
 *
 * \file BLV_event.c
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#include "BLV_event.h"

SDL_bool BLV_manage_event(BLV_Keyboard *keyboard)
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT:
            return SDL_FALSE;
        case SDL_TEXTINPUT:
            // SDL_Log("Text input : %s", event->text.text);
            break;
        case SDL_TEXTEDITING:
            // SDL_Log("Text editing : %s", event->edit.text);
            break;
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
            case 'a':
                keyboard->key['a'] = SDL_TRUE;
                break;
            case 'z':
                keyboard->key['z'] = SDL_TRUE;
                break;
            case 'e':
                keyboard->key['e'] = SDL_TRUE;
                break;
            case 'r':
                keyboard->key['r'] = SDL_TRUE;
                break;
            case 't':
                keyboard->key['t'] = SDL_TRUE;
                break;
            case 'y':
                keyboard->key['y'] = SDL_TRUE;
                break;
            case 'u':
                keyboard->key['u'] = SDL_TRUE;
                break;
            case 'i':
                keyboard->key['i'] = SDL_TRUE;
                break;
            case 'o':
                keyboard->key['o'] = SDL_TRUE;
                break;
            case 'p':
                keyboard->key['p'] = SDL_TRUE;
                break;
            case 'q':
                keyboard->key['q'] = SDL_TRUE;
                break;
            case 's':
                keyboard->key['s'] = SDL_TRUE;
                break;
            case 'd':
                keyboard->key['d'] = SDL_TRUE;
                break;
            case 'f':
                keyboard->key['f'] = SDL_TRUE;
                break;
            case 'g':
                keyboard->key['g'] = SDL_TRUE;
                break;
            case 'h':
                keyboard->key['h'] = SDL_TRUE;
                break;
            case 'j':
                keyboard->key['j'] = SDL_TRUE;
                break;
            case 'k':
                keyboard->key['k'] = SDL_TRUE;
                break;
            case 'l':
                keyboard->key['l'] = SDL_TRUE;
                break;
            case 'm':
                keyboard->key['m'] = SDL_TRUE;
                break;
            case 'w':
                keyboard->key['w'] = SDL_TRUE;
                break;
            case 'x':
                keyboard->key['x'] = SDL_TRUE;
                break;
            case 'c':
                keyboard->key['c'] = SDL_TRUE;
                break;
            case 'v':
                keyboard->key['v'] = SDL_TRUE;
                break;
            case 'b':
                keyboard->key['b'] = SDL_TRUE;
                break;
            case 'n':
                keyboard->key['n'] = SDL_TRUE;
                break;
            case ' ':
                keyboard->key[' '] = SDL_TRUE;
                break;
            }
            break;
        case SDL_KEYUP:
            switch (event.key.keysym.sym)
            {
            case 'a':
                keyboard->key['a'] = SDL_FALSE;
                break;
            case 'z':
                keyboard->key['z'] = SDL_FALSE;
                break;
            case 'e':
                keyboard->key['e'] = SDL_FALSE;
                break;
            case 'r':
                keyboard->key['r'] = SDL_FALSE;
                break;
            case 't':
                keyboard->key['t'] = SDL_FALSE;
                break;
            case 'y':
                keyboard->key['y'] = SDL_FALSE;
                break;
            case 'u':
                keyboard->key['u'] = SDL_FALSE;
                break;
            case 'i':
                keyboard->key['i'] = SDL_FALSE;
                break;
            case 'o':
                keyboard->key['o'] = SDL_FALSE;
                break;
            case 'p':
                keyboard->key['p'] = SDL_FALSE;
                break;
            case 'q':
                keyboard->key['q'] = SDL_FALSE;
                break;
            case 's':
                keyboard->key['s'] = SDL_FALSE;
                break;
            case 'd':
                keyboard->key['d'] = SDL_FALSE;
                break;
            case 'f':
                keyboard->key['f'] = SDL_FALSE;
                break;
            case 'g':
                keyboard->key['g'] = SDL_FALSE;
                break;
            case 'h':
                keyboard->key['h'] = SDL_FALSE;
                break;
            case 'j':
                keyboard->key['j'] = SDL_FALSE;
                break;
            case 'k':
                keyboard->key['k'] = SDL_FALSE;
                break;
            case 'l':
                keyboard->key['l'] = SDL_FALSE;
                break;
            case 'm':
                keyboard->key['m'] = SDL_FALSE;
                break;
            case 'w':
                keyboard->key['w'] = SDL_FALSE;
                break;
            case 'x':
                keyboard->key['x'] = SDL_FALSE;
                break;
            case 'c':
                keyboard->key['c'] = SDL_FALSE;
                break;
            case 'v':
                keyboard->key['v'] = SDL_FALSE;
                break;
            case 'b':
                keyboard->key['b'] = SDL_FALSE;
                break;
            case 'n':
                keyboard->key['n'] = SDL_FALSE;
                break;
            case ' ':
                keyboard->key[' '] = SDL_FALSE;
                break;
            }
            break;
        }
    }

    return SDL_TRUE;
}
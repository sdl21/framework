/**
 * \brief contain all code implementation for manage font and text
 *
 * \file BLV_textProcessing.c
 * \author Gautier Levesque
 * \date 09-09-2024
 */

#include "BLV_textProcessing.h"

#include <dirent.h>
#include <stdint.h>

#include "BLV_util.h"

SDL_bool BLV_create_all_font(const char *path, BLV_Font_array *fonts)
{
    SDL_Log("Try to create all font");

    if (fonts == NULL)
    {
        SDL_Log("Failed to create all font, cause fonts is NULL");
        return SDL_FALSE;
    }

    if (path == NULL)
    {
        SDL_Log("Failed to create all font, cause path is NULL");
        return SDL_FALSE;
    }

    DIR *open_dir = opendir(path);
    if (open_dir == NULL)
    {
        SDL_Log("Failed to create font, cause opendir failed with path : %s", path);
        return SDL_FALSE;
    }

    struct dirent *file;

    char buf[100];
    memset(buf, 0, 100);
    BLV_get_formated_date(buf, sizeof(buf));
    SDL_Log("Starting creating all font at : %s", buf);

    uint64_t start_time = SDL_GetTicks64();
    while ((file = readdir(open_dir)) != NULL)
    {
        if (strcmp(file->d_name, ".") == 0 || strcmp(file->d_name, "..") == 0)
            continue;

        char path_to_font[255];
        memset(path_to_font, 0, sizeof(path_to_font));

        if(strlen(path) + strlen(file->d_name) > 254)
            continue;

        // get path for font
        memcpy(path_to_font, path, strlen(path) > (255 - strlen(file->d_name)) ? (255 - strlen(file->d_name)) : strlen(path));
        memcpy(path_to_font + strlen(path), file->d_name, strlen(file->d_name));

        // fill name for font
        memcpy(fonts->fonts[fonts->font_count].name, file->d_name, strlen(file->d_name));
        // creating font
        fonts->fonts[fonts->font_count].font = TTF_OpenFont(path_to_font, 10);
        fonts->fonts[fonts->font_count].size = 10;
        if (fonts->fonts[fonts->font_count].font == NULL)
        {
            SDL_Log("Failed to create all font, cause TTF_OpenFont failed");
            closedir(open_dir);
            open_dir = NULL;
            return SDL_FALSE;
        }
        fonts->fonts[fonts->font_count++].active = SDL_TRUE;
    }
    uint64_t end_time = SDL_GetTicks64();

    memset(buf, 0, 100);
    BLV_get_formated_date(buf, sizeof(buf));
    SDL_Log("Ending creating all font at : %s, took time : %ld millisecondes", buf, end_time - start_time);

    // close directory
    closedir(open_dir);
    open_dir = NULL;

    SDL_Log("Create all font success");
    return SDL_TRUE;
}

SDL_bool BLV_clean_all_fonts(BLV_Font_array *fonts)
{
    SDL_Log("Try to clean all fonts");

    if (fonts == NULL)
    {
        SDL_Log("Failed to clean all fonts, cause fonts is NULL");
        return SDL_FALSE;
    }

    for (uint8_t i = 0; i < fonts->font_count; ++i)
    {
        if (fonts->fonts[i].font == NULL)
            continue;

        TTF_CloseFont(fonts->fonts[i].font);
        fonts->fonts[i].font = NULL;
    }

    memset(fonts, 0, sizeof(BLV_Font_array));

    SDL_Log("Clean all fonts success");
    return SDL_TRUE;
}

SDL_bool BLV_get_font(const char *font_path, BLV_Font **font, uint8_t size, BLV_Font_array *fonts)
{
    if (fonts == NULL)
    {
        SDL_Log("Failed to get font, cause r is NULL");
        return SDL_FALSE;
    }
    if (font_path == NULL)
    {
        SDL_Log("Failed to get font, cause font_path is NULL");
        return SDL_FALSE;
    }

    SDL_bool found = SDL_FALSE;
    for (uint8_t i = 0; i < fonts->font_count; ++i)
    {
        if (strcmp(font_path, fonts->fonts[i].name) == 0)
        {
            found = SDL_TRUE;
            fonts->fonts[i].size = size;
            if(TTF_SetFontSize(fonts->fonts[i].font, fonts->fonts[i].size) < 0)
            {
                SDL_Log("Failed to change font size");
                return SDL_FALSE;
            }
            *font = &fonts->fonts[i];
            break;
        }
    }

    return found;
}

SDL_bool BLV_create_text(const char *text, BLV_Texture *texture, BLV_Font *font, BLV_Renderer *r, SDL_Color color)
{
    if(text == NULL)
    {
        SDL_Log("Failed to create text, cause text is NULL");
        return SDL_FALSE;
    }
    if(texture == NULL)
    {
        SDL_Log("Failed to create text, cause texture is NULL");
        return SDL_FALSE;
    }
    if(font == NULL)
    {
        SDL_Log("Failed to create text, cause font is NULL");
        return SDL_FALSE;
    }
    if( r == NULL)
    {
        SDL_Log("Failed to create text, cause enderer is  NULL");
        return SDL_FALSE;
    }

    memcpy(texture->name, "TextToTexture", sizeof(texture->name));

    SDL_Surface *surface = TTF_RenderText_Solid(font->font, text, color);
    if(surface == NULL)
    {
        SDL_Log("Error when create surface wrapped solid in create text : %s", TTF_GetError());
        return SDL_FALSE;
    }
    
    texture->texture = SDL_CreateTextureFromSurface(r->renderer, surface);
    free(surface);
    surface = NULL;

    if(texture->texture == NULL)
    {
        SDL_Log("Error when create texture from surface in create text : %s", SDL_GetError());
        return SDL_FALSE;
    }

    return SDL_TRUE;
}

SDL_bool BLV_clean_text(BLV_Texture *txt)
{
    if(txt != NULL && txt->texture != NULL)
    {
        SDL_DestroyTexture(txt->texture);
        txt->texture = NULL;
    }
    
    return SDL_TRUE;
}
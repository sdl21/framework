/**
 * \brief main file contain all code logic for start a window in SDL2
 *
 * \file main.c
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#include <SDL_mixer.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <time.h>

#include "BLV_building.h"
#include "BLV_window.h"
#include "BLV_graphic.h"
#include "BLV_event.h"
#include "BLV_draw.h"
#include "BLV_textProcessing.h"
#include "BLV_map.h"
#include "BLV_army.h"
#include "BLV_gui.h"

/**
 * \brief use to define how many FPS we want in process
 */
#define FPS 60

/**
 *\brief use to set render draw color to default
 */
static SDL_Color color_black = {0, 0, 0, 0};

/**
 * \brief use to draw all thing on screen
 *
 * \param renderer renderer to draw all thing
 * \param map map to draw
 * \param fps_texture text who print fps count
 * \param is_in_battlefield if a battle if occur
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool BLV_draw(BLV_Renderer renderer, BLV_Map map, BLV_Army_array armies, BLV_Texture *fps_texture, State_of_game state, BLV_Panel *lst_panel, int drawing_offsetX, int drawing_offsetY);

/**
 * \brief main function
 *
 * \param argc number of arugment passed to exe
 * \param argv string represent each argument
 *
 * \param return 0 on success, negative code on error
 */
int main(int argc, char *argv[])
{
    // to remove warning
    for (uint8_t i = 0; i < argc; ++i)
        printf("Argv[%d] = %s\r\n", i, argv[i]);

    srand(time(NULL));
    // time variable
    Uint32 current_time = SDL_GetTicks();
    Uint32 past_time = SDL_GetTicks();
    const Uint32 fps = 1000 / FPS;

    // core thing
    BLV_Window window;
    BLV_Renderer renderer;
    BLV_Font_array fonts;
    BLV_Panel panel_list[MAX_STATE];
    BLV_Keyboard keyboard;
    BLV_Map map;
    BLV_Army_array armies;
    BLV_Mouse mouse;
    BLV_Building_array buildings;
    BLV_Animation animations[MAX_ANIMATION];
    State_of_game state = MAIN_MENU_STATE;

    // initialization
    SDL_bool launch = SDL_TRUE;
    launch = launch && (SDL_Init(SDL_INIT_EVERYTHING) < 0 ? SDL_FALSE : SDL_TRUE);
    launch = launch && (IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) < 0 ? SDL_FALSE : SDL_TRUE);
    launch = launch && (TTF_Init() < 0 ? SDL_FALSE : SDL_TRUE);
    launch = launch && (Mix_Init(MIX_INIT_MP3) == 0 ? SDL_FALSE : SDL_TRUE);

    SDL_Log("SDL version : %d.%d.%d\n", SDL_MAJOR_VERSION, SDL_MINOR_VERSION, SDL_PATCHLEVEL);
    SDL_Log("SDL ttf version : %d.%d.%d\n", SDL_TTF_MAJOR_VERSION, SDL_TTF_MINOR_VERSION, SDL_TTF_PATCHLEVEL);
    SDL_Log("SDL image version : %d.%d.%d\n", SDL_IMAGE_MAJOR_VERSION, SDL_IMAGE_MINOR_VERSION, SDL_IMAGE_PATCHLEVEL);
    SDL_Log("SDL mixer version: %d.%d.%d\n", SDL_MIXER_MAJOR_VERSION, SDL_MIXER_MINOR_VERSION, SDL_MIXER_PATCHLEVEL);

    launch = launch && BLV_create_BLV_window("./config.txt", &window);
    launch = launch && BLV_create_BLV_renderer(window.window, &renderer);
    launch = launch && BLV_create_all_font("./fonts/", &fonts);
    launch = launch && BLV_create_array_of_BLV_animation(animations, &renderer);
    launch = launch && BLV_create_all_panel(&renderer, window, panel_list, &fonts, animations);
    launch = launch && BLV_init_map(&map, animations, window.width / TILE_WIDTH, window.height / TILE_HEIGHT - 1);
    memset(keyboard.key, SDL_FALSE, sizeof(SDL_bool) * MAX_KEY);
    memset(&mouse, 0, sizeof(BLV_Mouse));
    SDL_Log("Init core thing : %d", launch);

    buildings.building_count = 0;
    buildings.max_building = 20;
    buildings.buildings = malloc(sizeof(BLV_Building) * buildings.max_building);
    if (buildings.buildings == NULL)
        launch = SDL_FALSE;
    else
        memset(buildings.buildings, 0, sizeof(BLV_Building) * buildings.max_building);
    launch = launch && BLV_init_building(&map, animations, &buildings.buildings[buildings.building_count++], TYPE_BUILDING_CASTLE, PLAYER_TEAM, 0, 0);
    launch = launch && BLV_init_building(&map, animations, &buildings.buildings[buildings.building_count++], TYPE_BUILDING_CASTLE, ENNEMY_TEAM, map.width - 1, map.height - 1);
    SDL_Log("Init building %d", launch);

    // for print FPS
    BLV_Texture fps_texture;
    SDL_Color color_white = {255, 255, 255, 255};
    BLV_Font *font;
    if (BLV_get_font("comici.ttf", &font, 10, &fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        launch = SDL_FALSE;
    }
    launch = launch && BLV_create_text("Unknow", &fps_texture, font, &renderer, color_white);
    SDL_Log("Creating fps text : %d", launch);

    // to manage FPS
    uint32_t time_last_FPS_show = SDL_GetTicks();
    uint8_t tick_passed = 0;
    //uint32_t dt = 0;
    SDL_bool generateArmyStat = SDL_FALSE;

    // todo remove
    uint32_t idx_manage_castel = 0;

    int drawing_offsetX = window.width / 2 - TILE_WIDTH * map.width / 2;
    int drawing_offsetY = window.height / 2 - TILE_HEIGHT * map.height / 2;

    SDL_Log("start game : %d", launch);
    // main loop
    while (launch == SDL_TRUE)
    {
        current_time = SDL_GetTicks();
        // if we need to update application
        if (current_time - past_time < fps)
            continue;

        mouse.left_click_pressed = SDL_GetMouseState(&mouse.x, &mouse.y) & SDL_BUTTON(SDL_BUTTON_LEFT);
        mouse.right_click_pressed = (SDL_GetMouseState(&mouse.x, &mouse.y) & SDL_BUTTON(SDL_BUTTON_RIGHT)) == 4 ? SDL_TRUE : SDL_FALSE;

        launch = launch && BLV_manage_event(&keyboard);
        launch = launch && BLV_manage_gui(panel_list, animations, &state, &map, &buildings, &renderer, &armies, idx_manage_castel, &fonts, mouse);

        if (state == QUIT_GAME)
            break;
        //dt = current_time - past_time;
        if (state == BATTLEFIELD_STATE)
        {
        
        }
        else if (state == MAP_STATE)
        {
            SDL_bool ennemy_alive = SDL_FALSE;
            SDL_bool ally_alive = SDL_FALSE;
            for (uint8_t i = 0; i < armies.armies_count; ++i)
            {
                launch = launch && BLV_update_army(armies, &armies.armies[i], &map, drawing_offsetX, drawing_offsetY, &state, &generateArmyStat, mouse);
                if (generateArmyStat == SDL_TRUE)
                {
                    if (BLV_get_font("comici.ttf", &font, 20, &fonts) == SDL_FALSE)
                    {
                        SDL_Log("Failed to get a font");
                        launch = SDL_FALSE;
                    }
                    BLV_clean_text(&panel_list[5].lst_label[1].txt_iddle);
                    BLV_clean_text(&panel_list[5].lst_label[2].txt_iddle);
                    BLV_clean_text(&panel_list[5].lst_label[3].txt_iddle);
                    BLV_clean_text(&panel_list[5].lst_label[4].txt_iddle);
                    BLV_clean_text(&panel_list[5].lst_label[5].txt_iddle);
                    SDL_Color white = {255, 255, 255, 255};
                    char str[20];
                    sprintf(str, "%d", armies.armies[i].count_of_npc[WARRIOR]);
                    BLV_create_text(str, &panel_list[5].lst_label[1].txt_iddle, font, &renderer, white);
                    sprintf(str, "%d", armies.armies[i].count_of_npc[KNIGHT]);
                    BLV_create_text(str, &panel_list[5].lst_label[2].txt_iddle, font, &renderer, white);
                    sprintf(str, "%d", armies.armies[i].count_of_npc[THIEF]);
                    BLV_create_text(str, &panel_list[5].lst_label[3].txt_iddle, font, &renderer, white);
                    sprintf(str, "%d", armies.armies[i].count_of_npc[MARKSMAN]);
                    BLV_create_text(str, &panel_list[5].lst_label[4].txt_iddle, font, &renderer, white);
                    sprintf(str, "%d", armies.armies[i].count_of_npc[WIZARD]);
                    BLV_create_text(str, &panel_list[5].lst_label[5].txt_iddle, font, &renderer, white);
                    generateArmyStat = SDL_FALSE;
                }
                if (armies.armies[i].army_team == 0 && armies.armies[i].is_active == SDL_TRUE)
                    ally_alive = SDL_TRUE;
                else if (armies.armies[i].army_team == 1 && armies.armies[i].is_active == SDL_TRUE)
                    ennemy_alive = SDL_TRUE;
            }

            if (ally_alive == SDL_FALSE)
                state = LOOSE_STATE;
            else if (ennemy_alive == SDL_FALSE)
                state = WIN_STATE;

            SDL_bool alreadyLoad = SDL_FALSE;
            for (uint16_t i = 0; i < buildings.building_count; ++i)
            {
                launch = launch && BLV_update_building(&buildings.buildings[i], &map, armies, mouse, drawing_offsetX, drawing_offsetY, &state);
                if (alreadyLoad == SDL_FALSE && state == SEE_CASTLE_STATE)
                {
                    if (BLV_get_font("comici.ttf", &font, 20, &fonts) == SDL_FALSE)
                    {
                        SDL_Log("Failed to get a font");
                        launch = SDL_FALSE;
                    }
                    BLV_clean_text(&panel_list[SEE_CASTLE_STATE].lst_label[1].txt_iddle);
                    BLV_clean_text(&panel_list[SEE_CASTLE_STATE].lst_label[2].txt_iddle);
                    BLV_clean_text(&panel_list[SEE_CASTLE_STATE].lst_label[3].txt_iddle);
                    BLV_clean_text(&panel_list[SEE_CASTLE_STATE].lst_label[4].txt_iddle);
                    BLV_clean_text(&panel_list[SEE_CASTLE_STATE].lst_label[5].txt_iddle);
                    SDL_Color white = {255, 255, 255, 255};
                    char str[20];
                    sprintf(str, "%d", buildings.buildings[i].count_of_npc[WARRIOR]);
                    BLV_create_text(str, &panel_list[SEE_CASTLE_STATE].lst_label[1].txt_iddle, font, &renderer, white);
                    sprintf(str, "%d", buildings.buildings[i].count_of_npc[KNIGHT]);
                    BLV_create_text(str, &panel_list[SEE_CASTLE_STATE].lst_label[2].txt_iddle, font, &renderer, white);
                    sprintf(str, "%d", buildings.buildings[i].count_of_npc[THIEF]);
                    BLV_create_text(str, &panel_list[SEE_CASTLE_STATE].lst_label[3].txt_iddle, font, &renderer, white);
                    sprintf(str, "%d", buildings.buildings[i].count_of_npc[MARKSMAN]);
                    BLV_create_text(str, &panel_list[SEE_CASTLE_STATE].lst_label[4].txt_iddle, font, &renderer, white);
                    sprintf(str, "%d", buildings.buildings[i].count_of_npc[WIZARD]);
                    BLV_create_text(str, &panel_list[SEE_CASTLE_STATE].lst_label[5].txt_iddle, font, &renderer, white);
                    alreadyLoad = SDL_TRUE;
                }
                else if (alreadyLoad == SDL_FALSE && state == MANAGE_CASTLE_STATE)
                {
                    idx_manage_castel = buildings.buildings[i].pos.x + buildings.buildings[i].pos.y * map.width;
                    BLV_Army *army = (BLV_Army *)map.map[buildings.buildings[i].pos.x + buildings.buildings[i].pos.y * map.width].playerArmy;
                    alreadyLoad = BLV_generate_manage_castel_text(&renderer, army, buildings.buildings[i], &fonts, &panel_list[MANAGE_CASTLE_STATE]);
                }
            }
        }

        launch = launch && BLV_update_map(/*&map, dt, state*/);
        launch = launch && BLV_draw(renderer, map, armies, &fps_texture, state, panel_list, drawing_offsetX, drawing_offsetY);

        if (time_last_FPS_show + 1000 < SDL_GetTicks())
        {
            char fps_text[255];
            memset(fps_text, 0, 255);
            sprintf(fps_text, "FPS : %d", tick_passed);
            BLV_clean_text(&fps_texture);
            BLV_create_text(fps_text, &fps_texture, font, &renderer, color_white);

            tick_passed = 0;
            time_last_FPS_show = SDL_GetTicks();
        }
        else
            tick_passed += 1;

        mouse.old_left_click_pressed = mouse.left_click_pressed;
        mouse.old_right_click_pressed = mouse.right_click_pressed;
        past_time = SDL_GetTicks();
    }

    // clean time
    SDL_bool clean_success = SDL_TRUE;
    for (uint8_t i = 0; i < MAX_ANIMATION; ++i)
    {
        clean_success = BLV_clean_BLV_animation(&animations[i]) && clean_success;
    }
    for (uint8_t i = 0; i < MAX_STATE; ++i)
    {
        clean_success = BLV_clean_panel(&panel_list[i]) && clean_success;
    }
    clean_success = BLV_clean_building(&buildings) && clean_success;
    clean_success = BLV_clean_text(&fps_texture) && clean_success;
    clean_success = BLV_clean_map(&map) && clean_success;
    clean_success = BLV_clean_BLV_Renderer(&renderer) && clean_success;
    clean_success = BLV_clean_BLV_window(&window) && clean_success;

    for (uint8_t i = 0; i < armies.armies_count; ++i)
    {
        clean_success = BLV_clean_army(&armies.armies[i]) && clean_success;
    }
    clean_success = BLV_clean_all_fonts(&fonts) && clean_success;

    if (clean_success == SDL_FALSE)
        SDL_Log("Clean operation failed");
    else
        SDL_Log("Clean operation success");

    Mix_Quit();
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();

    // its okay
    return 0;
}

//SDL_bool BLV_draw_battlefield(BLV_Renderer renderer, BLV_Map map, uint32_t battle_tile)
//{
    
//}

SDL_bool BLV_draw_campaign_map(BLV_Renderer renderer, BLV_Map map, BLV_Army_array armies, int drawing_offsetX, int drawing_offsetY)
{
    for (uint32_t i = 0; i < map.height; ++i)
    {
        for (uint32_t j = 0; j < map.width; ++j)
        {
            uint32_t idx = i * map.width + j;
            if (map.map[idx].tile.texture == NULL)
            {
                if (BLV_draw_rect(renderer.renderer, map.map[idx].pos) == SDL_FALSE)
                    return SDL_FALSE;
            }
            else
            {
                BLV_Animation *animMap = &map.map[idx].tile;
                SDL_Rect r;
                memcpy(&r, &map.map[idx].pos.r, sizeof(SDL_Rect));
                r.x += drawing_offsetX;
                r.y += drawing_offsetY;
                if (BLV_draw_textureEx(renderer.renderer, animMap->texture->texture, &animMap->src[animMap->currentFramePlayed], &r, 0, NULL, animMap->flip) == SDL_FALSE)
                    return SDL_FALSE;
            }

            if (map.map[idx].building != NULL)
            {
                BLV_Building *b = (BLV_Building *)map.map[idx].building;
                SDL_Rect r;
                r.x = b->pos.x * TILE_WIDTH + drawing_offsetX;
                r.y = b->pos.y * TILE_HEIGHT + drawing_offsetY;
                r.w = b->pos.w;
                r.h = b->pos.h;

                if (BLV_draw_texture(renderer.renderer, b->animations[b->current_animation]->texture->texture, NULL, &r) == SDL_FALSE)
                    return SDL_FALSE;
            }

            if (map.map[idx].can_move == SDL_TRUE)
            {
                BLV_Rect canMoveRect;
                SDL_Color lightGreen = {0, 255, 0, 150};
                canMoveRect.r.x = map.map[idx].pos.r.x + drawing_offsetX;
                canMoveRect.r.y = map.map[idx].pos.r.y + drawing_offsetY;
                canMoveRect.r.w = map.map[idx].pos.r.w;
                canMoveRect.r.h = map.map[idx].pos.r.h;
                canMoveRect.color = lightGreen;
                canMoveRect.fill = SDL_TRUE;
                if (BLV_draw_rect(renderer.renderer, canMoveRect) == SDL_FALSE)
                    return SDL_FALSE;
            }
        }
    }

    for (uint8_t i = 0; i < armies.armies_count; ++i)
    {
        if (armies.armies[i].is_active == SDL_FALSE)
            continue;

        BLV_Army army = armies.armies[i];
        BLV_Animation *anim = army.animation;
        army.pos.x *= TILE_WIDTH;
        army.pos.y *= TILE_HEIGHT;
        SDL_Rect r;
        memcpy(&r, &army.pos, sizeof(SDL_Rect));
        r.x += drawing_offsetX;
        r.y += drawing_offsetY;
        if (BLV_draw_textureEx(renderer.renderer, anim->texture->texture, &anim->src[anim->currentFramePlayed], &r, 0, NULL, anim->flip) == SDL_FALSE)
            return SDL_FALSE;

        if (army.army_team == 0)
        {
            BLV_Rect rectTeam0;
            SDL_Color green = {0, 255, 0, 175};
            rectTeam0.r.x = army.pos.x + drawing_offsetX;
            rectTeam0.r.y = army.pos.y + army.pos.h - 10 + drawing_offsetY;
            rectTeam0.r.w = 10;
            rectTeam0.r.h = 10;
            rectTeam0.color = green;
            rectTeam0.fill = SDL_TRUE;
            if (BLV_draw_rect(renderer.renderer, rectTeam0) == SDL_FALSE)
                return SDL_FALSE;
        }
        else
        {
            BLV_Rect rectTeam1;
            SDL_Color red = {255, 0, 0, 175};
            rectTeam1.r.x = army.pos.x + drawing_offsetX;
            rectTeam1.r.y = army.pos.y + army.pos.h - 10 + drawing_offsetY;
            rectTeam1.r.w = 10;
            rectTeam1.r.h = 10;
            rectTeam1.color = red;
            rectTeam1.fill = SDL_TRUE;
            if (BLV_draw_rect(renderer.renderer, rectTeam1) == SDL_FALSE)
                return SDL_FALSE;
        }

        if (army.selected == SDL_TRUE)
        {
            BLV_Rect selectedRect;
            SDL_Color lightGray = {150, 150, 150, 150};
            selectedRect.r.x = army.pos.x + drawing_offsetX;
            selectedRect.r.y = army.pos.y + drawing_offsetY;
            selectedRect.r.w = army.pos.w;
            selectedRect.r.h = army.pos.h;
            selectedRect.color = lightGray;
            selectedRect.fill = SDL_TRUE;
            if (BLV_draw_rect(renderer.renderer, selectedRect) == SDL_FALSE)
                return SDL_FALSE;
        }
    }
    return SDL_TRUE;
}

SDL_bool BLV_draw_panel(BLV_Renderer renderer, BLV_Panel panel)
{
    for (uint8_t j = 0; j < panel.panel_picture_count; ++j)
    {
        BLV_Panel_picture *picture = &panel.lst_panel_picture[j];

        if (picture->active == SDL_FALSE)
            continue;

        BLV_Animation *anim = picture->animation;
        SDL_Rect src = anim->src[anim->animationWay + anim->animationType + anim->currentFramePlayed];
        if (BLV_draw_texture(renderer.renderer, picture->animation->texture->texture, &src, &picture->pos.r) == SDL_FALSE)
            return SDL_FALSE;
    }

    for (uint8_t i = 0; i < panel.button_count; ++i)
    {
        BLV_Button *btn = &panel.lst_btn[i];

        if (btn->is_pressed == SDL_TRUE)
        {
            if (BLV_draw_texture(renderer.renderer, btn->animationPressed->texture->texture, NULL, &btn->rectPressed.r) == SDL_FALSE)
                return SDL_FALSE;
            if (BLV_draw_texture(renderer.renderer, btn->text.txt_pressed.texture, NULL, &btn->text.pos_pressed.r) == SDL_FALSE)
                return SDL_FALSE;
        }
        else if (btn->is_hoover == SDL_TRUE)
        {
            if (BLV_draw_texture(renderer.renderer, btn->animationHoover->texture->texture, NULL, &btn->rectHoover.r) == SDL_FALSE)
                return SDL_FALSE;
            if (BLV_draw_texture(renderer.renderer, btn->text.txt_hoover.texture, NULL, &btn->text.pos_hoover.r) == SDL_FALSE)
                return SDL_FALSE;
        }
        else
        {
            if (BLV_draw_texture(renderer.renderer, btn->animationIddle->texture->texture, NULL, &btn->rectIddle.r) == SDL_FALSE)
                return SDL_FALSE;
            if (BLV_draw_texture(renderer.renderer, btn->text.txt_iddle.texture, NULL, &btn->text.pos_iddle.r) == SDL_FALSE)
                return SDL_FALSE;
        }
    }

    for (uint8_t i = 0; i < panel.label_count; ++i)
    {
        BLV_Label *lbl = &panel.lst_label[i];

        if (lbl->is_active == SDL_FALSE)
            continue;

        if (BLV_draw_texture(renderer.renderer, lbl->txt_iddle.texture, NULL, &lbl->pos_iddle.r) == SDL_FALSE)
            return SDL_FALSE;
    }

    return SDL_TRUE;
}

SDL_bool BLV_draw(BLV_Renderer renderer, BLV_Map map, BLV_Army_array armies, BLV_Texture *fps_texture, State_of_game state, BLV_Panel *lst_panel, int drawing_offsetX, int drawing_offsetY)
{
    if (renderer.renderer == NULL)
    {
        SDL_Log("Warning can't draw thing on screen, cause renderer is NULL");
        return SDL_TRUE;
    }

    if (BLV_set_renderer_draw_color(renderer.renderer, color_black) == SDL_FALSE)
        return SDL_FALSE;

    if (SDL_RenderClear(renderer.renderer) < 0)
    {
        SDL_Log("Failed to SDL_RenderClear, cause %s", SDL_GetError());
        return SDL_FALSE;
    }

    switch (state)
    {
    case BATTLEFIELD_STATE:
        if (BLV_draw_panel(renderer, lst_panel[BATTLEFIELD_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        //if (BLV_draw_battlefield(renderer, map, battle_tile) == SDL_FALSE)
            //return SDL_FALSE;
        break;
    case MAP_STATE:
        if (BLV_draw_panel(renderer, lst_panel[MAP_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        if (BLV_draw_campaign_map(renderer, map, armies, drawing_offsetX, drawing_offsetY) == SDL_FALSE)
            return SDL_FALSE;
        break;
    case MAIN_MENU_STATE:
        if (BLV_draw_panel(renderer, lst_panel[MAIN_MENU_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        break;
    case WIN_STATE:
        if (BLV_draw_panel(renderer, lst_panel[WIN_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        break;
    case LOOSE_STATE:
        if (BLV_draw_panel(renderer, lst_panel[LOOSE_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        break;
    case SEE_CASTLE_STATE:
        if (BLV_draw_panel(renderer, lst_panel[MAP_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        if (BLV_draw_campaign_map(renderer, map, armies, drawing_offsetX, drawing_offsetY) == SDL_FALSE)
            return SDL_FALSE;
        {
            BLV_Rect r;
            r.color = color_black;
            r.color.a = 100;
            r.fill = SDL_TRUE;
            r.r.x = 0;
            r.r.y = 0;
            r.r.w = 800;
            r.r.h = 600;
            if (BLV_draw_rect(renderer.renderer, r) == SDL_FALSE)
                return SDL_FALSE;
        }
        if (BLV_draw_panel(renderer, lst_panel[SEE_CASTLE_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        break;
    case SEE_ARMY_STATE:
        if (BLV_draw_panel(renderer, lst_panel[MAP_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        if (BLV_draw_campaign_map(renderer, map, armies, drawing_offsetX, drawing_offsetY) == SDL_FALSE)
            return SDL_FALSE;
        {
            BLV_Rect r;
            r.color = color_black;
            r.color.a = 100;
            r.fill = SDL_TRUE;
            r.r.x = 0;
            r.r.y = 0;
            r.r.w = 800;
            r.r.h = 600;
            if (BLV_draw_rect(renderer.renderer, r) == SDL_FALSE)
                return SDL_FALSE;
        }
        if (BLV_draw_panel(renderer, lst_panel[SEE_ARMY_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        break;
    case OPTION_MENU_MAP_STATE:
        if (BLV_draw_panel(renderer, lst_panel[MAP_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        if (BLV_draw_campaign_map(renderer, map, armies, drawing_offsetX, drawing_offsetY) == SDL_FALSE)
            return SDL_FALSE;
        {
            BLV_Rect r;
            r.color = color_black;
            r.color.a = 100;
            r.fill = SDL_TRUE;
            r.r.x = 0;
            r.r.y = 0;
            r.r.w = 800;
            r.r.h = 600;
            if (BLV_draw_rect(renderer.renderer, r) == SDL_FALSE)
                return SDL_FALSE;
        }
        if (BLV_draw_panel(renderer, lst_panel[OPTION_MENU_MAP_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        break;
    case MANAGE_CASTLE_STATE:
        if (BLV_draw_panel(renderer, lst_panel[MAP_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        if (BLV_draw_campaign_map(renderer, map, armies, drawing_offsetX, drawing_offsetY) == SDL_FALSE)
            return SDL_FALSE;
        {
            BLV_Rect r;
            r.color = color_black;
            r.color.a = 100;
            r.fill = SDL_TRUE;
            r.r.x = 0;
            r.r.y = 0;
            r.r.w = 800;
            r.r.h = 600;
            if (BLV_draw_rect(renderer.renderer, r) == SDL_FALSE)
                return SDL_FALSE;
        }
        if (BLV_draw_panel(renderer, lst_panel[MANAGE_CASTLE_STATE]) == SDL_FALSE)
            return SDL_FALSE;
        break;
    default:
        SDL_Log("State not manage for drawing");
    }

    static SDL_Rect place_of_fps = {755, 10, 40, 15};
    if (BLV_draw_texture(renderer.renderer, fps_texture->texture, NULL, &place_of_fps) == SDL_FALSE)
        return SDL_FALSE;

    SDL_RenderPresent(renderer.renderer);

    return SDL_TRUE;
}
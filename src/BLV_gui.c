/**
 * \brief gui file contain all code implementation for manage gui
 *
 * \file BLV_gui.c
 * \author Gautier Levesque
 * \date 08-12-2024
 */
#include "BLV_gui.h"

SDL_bool BLV_create_button(BLV_Renderer *renderer, BLV_Button *btn, BLV_Font *f, const char *txt, uint32_t x, uint32_t y, uint32_t width, uint32_t height, State_of_game state, BLV_button_type_enum type_button, SDL_Color txt_color_iddle, SDL_Color txt_color_hoover, SDL_Color txt_color_pressed, SDL_Color iddle_color, SDL_Color hoover_color, SDL_Color pressed_color, BLV_Animation *iddleAnimation, BLV_Animation *hooverAnimation, BLV_Animation *pressedAnimation)
{
    SDL_Rect place = {x, y, width, height};
    memcpy(&btn->rectHoover.r, &place, sizeof(SDL_Rect));
    memcpy(&btn->rectIddle.r, &place, sizeof(SDL_Rect));
    memcpy(&btn->rectPressed.r, &place, sizeof(SDL_Rect));

    if (txt != NULL)
    {
        if (BLV_create_label(renderer, &btn->text, f, txt, x, y, width, height, state, txt_color_iddle, txt_color_hoover, txt_color_pressed) == SDL_FALSE)
            return SDL_FALSE;
    }
    else
    {
        btn->text.txt_iddle.texture = NULL;
        btn->text.txt_hoover.texture = NULL;
        btn->text.txt_pressed.texture = NULL;
    }

    btn->rectIddle.color = iddle_color;
    btn->rectIddle.fill = SDL_FALSE;
    btn->rectHoover.color = hoover_color;
    btn->rectHoover.fill = SDL_TRUE;
    btn->rectPressed.color = pressed_color;
    btn->rectPressed.fill = SDL_TRUE;

    btn->animationIddle = malloc(sizeof(BLV_Animation));
    btn->animationHoover = malloc(sizeof(BLV_Animation));
    btn->animationPressed = malloc(sizeof(BLV_Animation));
    if (btn->animationIddle == NULL || btn->animationHoover == NULL || btn->animationPressed == NULL)
        return SDL_FALSE;

    memcpy(&btn->animationIddle, iddleAnimation, sizeof(BLV_Animation));
    memcpy(&btn->animationHoover, hooverAnimation, sizeof(BLV_Animation));
    memcpy(&btn->animationPressed, pressedAnimation, sizeof(BLV_Animation));

    SDL_Log("Pointer: %p,%p,%p,%p", btn->animationIddle, iddleAnimation,  btn->animationIddle->texture, iddleAnimation->texture);
    btn->animationIddle->texture = iddleAnimation->texture;
    btn->animationHoover->texture = hooverAnimation->texture;
    btn->animationPressed->texture = pressedAnimation->texture;

    btn->is_pressed = SDL_FALSE;
    btn->is_hoover = SDL_FALSE;
    btn->is_active = SDL_TRUE;
    btn->type_button = type_button;
    btn->state_of_active = state;

    return SDL_TRUE;
}

SDL_bool BLV_create_label(BLV_Renderer *renderer, BLV_Label *lbl, BLV_Font *f, const char *txt, uint32_t x, uint32_t y, uint32_t width, uint32_t height, State_of_game state, SDL_Color txt_color_iddle, SDL_Color txt_color_hoover, SDL_Color txt_color_pressed)
{
    int place_x, place_y, width_txt, height_txt;
    if (TTF_SizeText(f->font, txt, &width_txt, &height_txt) != 0)
    {
        SDL_Log("Failed to know size of text, so he will be upper left corner start");
        place_x = x;
        place_y = y;
    }
    else
    {
        place_x = x + width / 2 - width_txt / 2;
        place_y = y + height / 2 - height_txt / 2;
    }

    if (BLV_create_text(txt, &lbl->txt_iddle, f, renderer, txt_color_iddle) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_text(txt, &lbl->txt_hoover, f, renderer, txt_color_hoover) == SDL_FALSE)
        return SDL_FALSE;
    if (BLV_create_text(txt, &lbl->txt_pressed, f, renderer, txt_color_pressed) == SDL_FALSE)
        return SDL_FALSE;

    SDL_Rect pos = {place_x, place_y, width_txt, height_txt};

    memcpy(&lbl->pos_iddle.r, &pos, sizeof(SDL_Rect));
    memcpy(&lbl->pos_hoover.r, &pos, sizeof(SDL_Rect));
    memcpy(&lbl->pos_pressed.r, &pos, sizeof(SDL_Rect));

    lbl->pos_iddle.fill = SDL_FALSE;
    lbl->pos_hoover.fill = SDL_FALSE;
    lbl->pos_pressed.fill = SDL_FALSE;

    lbl->pos_iddle.color = txt_color_iddle;
    lbl->pos_hoover.color = txt_color_hoover;
    lbl->pos_pressed.color = txt_color_pressed;

    lbl->state_of_active = state;
    lbl->is_active = SDL_TRUE;

    return SDL_TRUE;
}

SDL_bool BLV_create_panel_picture(BLV_Panel_picture *picture, BLV_Animation *animation, uint32_t x, uint32_t y, uint32_t width, uint32_t height, State_of_game state)
{
    picture->pos.fill = SDL_FALSE;
    picture->pos.r.x = x;
    picture->pos.r.y = y;
    picture->pos.r.w = width;
    picture->pos.r.h = height;
    picture->active = SDL_TRUE;

    picture->animation = malloc(sizeof(BLV_Animation));
    if (picture->animation == NULL)
        return SDL_FALSE;

    memcpy(&picture->animation, animation, sizeof(BLV_Animation));
    // picture->animation = animation;
    picture->state = state;
    return SDL_TRUE;
}

SDL_bool BLV_create_panel(BLV_Panel *panel, uint8_t button_count, uint8_t label_count, uint8_t texture_count, State_of_game state)
{
    if (panel == NULL)
    {
        SDL_Log("Can't create panel because panel is NULL");
        return SDL_FALSE;
    }

    panel->is_active = SDL_TRUE;
    panel->state_of_active = state;
    panel->max_label = label_count;
    panel->max_button = button_count;
    panel->max_panel_picture = texture_count;
    panel->label_count = 0;
    panel->button_count = 0;
    panel->panel_picture_count = 0;

    panel->lst_label = calloc(label_count, sizeof(BLV_Label));
    panel->lst_btn = calloc(button_count, sizeof(BLV_Button));
    panel->lst_panel_picture = calloc(texture_count, sizeof(BLV_Texture));

    if (panel->lst_label == NULL)
    {
        SDL_Log("Can't create panel because malloc for label failed");
        return SDL_FALSE;
    }
    if (panel->lst_btn == NULL)
    {
        free(panel->lst_label);
        panel->lst_label = NULL;
        SDL_Log("Can't create panel because malloc for button failed");
        return SDL_FALSE;
    }
    if (panel->lst_btn == NULL)
    {
        free(panel->lst_label);
        panel->lst_label = NULL;
        free(panel->lst_btn);
        panel->lst_btn = NULL;
        SDL_Log("Can't create panel because malloc for texture failed");
        return SDL_FALSE;
    }

    return SDL_TRUE;
}

SDL_bool BLV_manage_gui(BLV_Panel *panel_list, BLV_Animation *animations, State_of_game *state, BLV_Map *m, BLV_Building_array *buildings, BLV_Renderer *renderer, BLV_Army_array *armies, uint32_t idx_manage_castel, BLV_Font_array *fonts, BLV_Mouse mouse)
{
    for (uint8_t i = 0; i < MAX_STATE; ++i)
    {
        BLV_Panel *panel = &panel_list[i];
        if (panel->is_active == SDL_FALSE || panel->state_of_active != *state)
            continue;

        for (uint8_t j = 0; j < panel->button_count; ++j)
        {
            BLV_Button *btn = &panel->lst_btn[j];
            if (btn->is_active == SDL_FALSE || btn->state_of_active != *state)
                continue;

            SDL_Point p = {mouse.x, mouse.y};
            btn->is_hoover = SDL_PointInRect(&p, &btn->rectIddle.r);
            if (mouse.left_click_pressed == SDL_TRUE && mouse.old_left_click_pressed == SDL_FALSE && btn->is_hoover == SDL_TRUE)
            {
                if (btn->is_pressed == SDL_FALSE)
                {
                    btn->is_pressed = SDL_TRUE;
                    switch (btn->type_button)
                    {
                    case END_TURN_BTN:
                        for (uint8_t j = 0; j < MAX_ARMY; ++j)
                        {
                            BLV_Army *army = &armies->armies[j];
                            if (army->is_active == SDL_TRUE && army->army_team > 0)
                                BLV_move_army(army, m);
                            army->move_remaining = BASE_MOVE;
                            if (army->selected == SDL_TRUE)
                            {
                                army->selected = SDL_FALSE;
                                BLV_reset_map_can_move(m);
                            }
                        }
                        BLV_building_make_recruitment(buildings);
                        break;
                    case START_GAME_BTN:
                        armies->armies_count = 8;

                        for (uint8_t i = 0; i < MAX_ARMY; ++i)
                            armies->armies[i].is_active = SDL_FALSE;

                        BLV_init_army(&armies->armies[0], animations, m, 0, 2, 2);
                        BLV_init_army(&armies->armies[1], animations, m, 0, 1, 4);
                        BLV_init_army(&armies->armies[2], animations, m, 0, 4, 4);
                        BLV_init_army(&armies->armies[3], animations, m, 0, 3, 9);

                        BLV_init_army(&armies->armies[4], animations, m, 1, 8, 8);
                        BLV_init_army(&armies->armies[5], animations, m, 1, 10, 13);
                        BLV_init_army(&armies->armies[6], animations, m, 1, 9, 11);
                        BLV_init_army(&armies->armies[7], animations, m, 1, 9, 10);
                        *state = MAP_STATE;
                        break;
                    case QUIT_GAME_BTN:
                        *state = QUIT_GAME;
                        break;
                    case BACK_TO_MAIN_MENU_WIN_BTN:
                        *state = MAIN_MENU_STATE;
                        break;
                    case BACK_TO_MAIN_MENU_LOOSE_BTN:
                        *state = MAIN_MENU_STATE;
                        break;
                    case LEAVE_SEE_ARMY_BTN:
                        *state = MAP_STATE;
                        break;
                    case RETURN_TO_MAP_FROM_OPTION_MENU_BTN:
                        *state = MAP_STATE;
                        break;
                    case GO_OPTION_MENU_MAP_BTN:
                        *state = OPTION_MENU_MAP_STATE;
                        break;
                    case BACK_TO_MAIN_MENU_OPTION_BTN:
                        *state = MAIN_MENU_STATE;
                        break;
                    case LEAVE_SEE_CASTLE_BTN:
                        *state = MAP_STATE;
                        break;
                    case LEAVE_MANAGE_CASTEL_BTN:
                        *state = MAP_STATE;
                        break;
                    case ADD_WARRIOR_TO_CASTEL_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (army->count_of_npc[WARRIOR] > 0)
                        {
                            army->count_of_npc[WARRIOR] -= 1;
                            building->count_of_npc[WARRIOR] += 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_KNIGHT_TO_CASTEL_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (army->count_of_npc[KNIGHT] > 0)
                        {
                            army->count_of_npc[KNIGHT] -= 1;
                            building->count_of_npc[KNIGHT] += 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_ROGUE_TO_CASTEL_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (army->count_of_npc[THIEF] > 0)
                        {
                            army->count_of_npc[THIEF] -= 1;
                            building->count_of_npc[THIEF] += 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_MARKSMAN_TO_CASTEL_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (army->count_of_npc[MARKSMAN] > 0)
                        {
                            army->count_of_npc[MARKSMAN] -= 1;
                            building->count_of_npc[MARKSMAN] += 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_WIZARD_TO_CASTEL_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (army->count_of_npc[WIZARD] > 0)
                        {
                            army->count_of_npc[WIZARD] -= 1;
                            building->count_of_npc[WIZARD] += 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_WARRIOR_TO_ARMY_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (building->count_of_npc[WARRIOR] > 0)
                        {
                            army->count_of_npc[WARRIOR] += 1;
                            building->count_of_npc[WARRIOR] -= 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_KNIGHT_TO_ARMY_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (building->count_of_npc[KNIGHT] > 0)
                        {
                            army->count_of_npc[KNIGHT] += 1;
                            building->count_of_npc[KNIGHT] -= 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_ROGUE_TO_ARMY_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (building->count_of_npc[THIEF] > 0)
                        {
                            army->count_of_npc[THIEF] += 1;
                            building->count_of_npc[THIEF] -= 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_MARKSMAN_TO_ARMY_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (building->count_of_npc[MARKSMAN] > 0)
                        {
                            army->count_of_npc[MARKSMAN] += 1;
                            building->count_of_npc[MARKSMAN] -= 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    case ADD_WIZARD_TO_ARMY_BTN:
                    {
                        BLV_Army *army = (BLV_Army *)m->map[idx_manage_castel].playerArmy;
                        BLV_Building *building = (BLV_Building *)m->map[idx_manage_castel].building;
                        if (building->count_of_npc[WIZARD] > 0)
                        {
                            army->count_of_npc[WIZARD] += 1;
                            building->count_of_npc[WIZARD] -= 1;
                            if (BLV_generate_manage_castel_text(renderer, army, *building, fonts, &panel_list[MANAGE_CASTLE_STATE]) == SDL_FALSE)
                                return SDL_FALSE;
                        }
                    }
                    break;
                    default:
                        SDL_Log("Button type not manage");
                    }
                }
            }
            else
            {
                btn->is_pressed = SDL_FALSE;
            }
        }
    }

    return SDL_TRUE;
}

SDL_bool BLV_generate_manage_castel_text(BLV_Renderer *renderer, BLV_Army *army, BLV_Building building, BLV_Font_array *fonts, BLV_Panel *panel)
{
    BLV_Font *font;
    if (BLV_get_font("comici.ttf", &font, 20, fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        return SDL_FALSE;
    }
    // generation des textes d'affichage pour le chateau
    BLV_clean_text(&panel->lst_label[1].txt_iddle);
    BLV_clean_text(&panel->lst_label[2].txt_iddle);
    BLV_clean_text(&panel->lst_label[3].txt_iddle);
    BLV_clean_text(&panel->lst_label[4].txt_iddle);
    BLV_clean_text(&panel->lst_label[5].txt_iddle);
    SDL_Color white = {255, 255, 255, 255};
    char str[20];
    sprintf(str, "%d", building.count_of_npc[WARRIOR]);
    BLV_create_text(str, &panel->lst_label[1].txt_iddle, font, renderer, white);
    sprintf(str, "%d", building.count_of_npc[KNIGHT]);
    BLV_create_text(str, &panel->lst_label[2].txt_iddle, font, renderer, white);
    sprintf(str, "%d", building.count_of_npc[THIEF]);
    BLV_create_text(str, &panel->lst_label[3].txt_iddle, font, renderer, white);
    sprintf(str, "%d", building.count_of_npc[MARKSMAN]);
    BLV_create_text(str, &panel->lst_label[4].txt_iddle, font, renderer, white);
    sprintf(str, "%d", building.count_of_npc[WIZARD]);
    BLV_create_text(str, &panel->lst_label[5].txt_iddle, font, renderer, white);

    // generation des textes d'affichage pour le joueur
    BLV_clean_text(&panel->lst_label[7].txt_iddle);
    BLV_clean_text(&panel->lst_label[8].txt_iddle);
    BLV_clean_text(&panel->lst_label[9].txt_iddle);
    BLV_clean_text(&panel->lst_label[10].txt_iddle);
    BLV_clean_text(&panel->lst_label[11].txt_iddle);
    sprintf(str, "%d", army->count_of_npc[WARRIOR]);
    BLV_create_text(str, &panel->lst_label[7].txt_iddle, font, renderer, white);
    sprintf(str, "%d", army->count_of_npc[KNIGHT]);
    BLV_create_text(str, &panel->lst_label[8].txt_iddle, font, renderer, white);
    sprintf(str, "%d", army->count_of_npc[THIEF]);
    BLV_create_text(str, &panel->lst_label[9].txt_iddle, font, renderer, white);
    sprintf(str, "%d", army->count_of_npc[MARKSMAN]);
    BLV_create_text(str, &panel->lst_label[10].txt_iddle, font, renderer, white);
    sprintf(str, "%d", army->count_of_npc[WIZARD]);
    BLV_create_text(str, &panel->lst_label[11].txt_iddle, font, renderer, white);

    return SDL_TRUE;
}

SDL_bool BLV_clean_panel(BLV_Panel *panel)
{
    if (panel == NULL)
        return SDL_TRUE;

    if (panel->lst_btn != NULL)
    {
        for (uint8_t i = 0; i < panel->button_count; ++i)
        {
            if (BLV_clean_button(&panel->lst_btn[i]) == SDL_FALSE)
                return SDL_FALSE;
        }

        free(panel->lst_btn);
        panel->lst_btn = NULL;
    }
    if (panel->lst_label != NULL)
    {
        for (uint8_t i = 0; i < panel->label_count; ++i)
        {
            if (BLV_clean_label(&panel->lst_label[i]) == SDL_FALSE)
                return SDL_FALSE;
        }

        free(panel->lst_label);
        panel->lst_label = NULL;
    }

    if (panel->lst_panel_picture != NULL)
    {
        free(panel->lst_panel_picture);
        panel->lst_panel_picture = NULL;
    }

    for (uint8_t i = 0; i < panel->panel_picture_count; ++i)
    {
        if (panel->lst_panel_picture[i].animation != NULL)
        {
            free(panel->lst_panel_picture[i].animation);
            panel->lst_panel_picture[i].animation = NULL;
        }
    }

    return SDL_TRUE;
}

SDL_bool BLV_clean_label(BLV_Label *lbl)
{
    SDL_bool no_error = SDL_TRUE;

    no_error = no_error && BLV_clean_text(&lbl->txt_iddle);
    no_error = no_error && BLV_clean_text(&lbl->txt_hoover);
    no_error = no_error && BLV_clean_text(&lbl->txt_pressed);

    return no_error;
}

SDL_bool BLV_clean_button(BLV_Button *btn)
{
    if (btn->animationIddle != NULL)
    {
        free(btn->animationIddle);
        btn->animationIddle = NULL;
    }
    if (btn->animationHoover != NULL)
    {
        free(btn->animationHoover);
        btn->animationHoover = NULL;
    }
    if (btn->animationPressed != NULL)
    {
        free(btn->animationPressed);
        btn->animationPressed = NULL;
    }
    return BLV_clean_label(&btn->text);
}

SDL_bool BLV_create_all_panel(BLV_Renderer *renderer, BLV_Window window, BLV_Panel *panel_list, BLV_Font_array *fonts, BLV_Animation *animations)
{
    SDL_bool launch = SDL_TRUE;

    for (uint8_t i = 0; i < MAX_STATE; ++i)
        memset(panel_list, 0, sizeof(BLV_Panel) * MAX_STATE);

    SDL_Color black = {0, 0, 0, 255};
    SDL_Color white = {255, 255, 255, 255};
    SDL_Color blue = {0, 0, 255, 255};
    BLV_Font *font;

    // init map state
    BLV_create_panel(&panel_list[MAP_STATE], 2, 0, 1, MAP_STATE);
    if (BLV_get_font("comici.ttf", &font, 20, fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        launch = SDL_FALSE;
    }
    launch = launch && BLV_create_button(renderer, &panel_list[MAP_STATE].lst_btn[panel_list[MAP_STATE].button_count++], font, "Fin du tour", 50, 565, 100, 30, MAP_STATE, END_TURN_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MAP_STATE].lst_btn[panel_list[MAP_STATE].button_count++], font, "Menu", 690, 565, 100, 30, MAP_STATE, GO_OPTION_MENU_MAP_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_panel_picture(&panel_list[MAP_STATE].lst_panel_picture[panel_list[MAP_STATE].panel_picture_count++], &animations[IMG_FOND_ANIMATION], 0, 0, window.width, window.height, MAP_STATE);

    // init main menu state
    BLV_create_panel(&panel_list[MAIN_MENU_STATE], 2, 1, 2, MAIN_MENU_STATE);
    if (BLV_get_font("comici.ttf", &font, 40, fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        launch = SDL_FALSE;
    }
    launch = launch && BLV_create_label(renderer, &panel_list[MAIN_MENU_STATE].lst_label[panel_list[MAIN_MENU_STATE].label_count++], font, "FIGHT FOR KINGDOM", 0, 0, window.width, 100, MAIN_MENU_STATE, white, white, white);
    launch = launch && BLV_create_button(renderer, &panel_list[MAIN_MENU_STATE].lst_btn[panel_list[MAIN_MENU_STATE].button_count++], font, "Lancer", window.width / 2 - 250 / 2, window.height / 2 - 75, 250, 50, MAIN_MENU_STATE, START_GAME_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MAIN_MENU_STATE].lst_btn[panel_list[MAIN_MENU_STATE].button_count++], font, "Quitter", window.width / 2 - 250 / 2, window.height / 2, 250, 50, MAIN_MENU_STATE, QUIT_GAME_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_panel_picture(&panel_list[MAIN_MENU_STATE].lst_panel_picture[panel_list[MAIN_MENU_STATE].panel_picture_count++], &animations[IMG_FOND_ANIMATION], 0, 0, window.width, window.height, MAIN_MENU_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[MAIN_MENU_STATE].lst_panel_picture[panel_list[MAIN_MENU_STATE].panel_picture_count++], &animations[BANNER_HANGING_ANIMATION], window.width / 2 - 250, 0, 500, 100, MAIN_MENU_STATE);

    // init win state
    BLV_create_panel(&panel_list[WIN_STATE], 1, 1, 1, WIN_STATE);
    if (BLV_get_font("comici.ttf", &font, 40, fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        launch = SDL_FALSE;
    }
    launch = launch && BLV_create_label(renderer, &panel_list[WIN_STATE].lst_label[panel_list[WIN_STATE].label_count++], font, "VICTOIRE", 0, 0, window.width, 50, WIN_STATE, white, white, white);
    if (BLV_get_font("comici.ttf", &font, 30, fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        launch = SDL_FALSE;
    }
    launch = launch && BLV_create_button(renderer, &panel_list[WIN_STATE].lst_btn[panel_list[WIN_STATE].button_count++], font, "Menu principal", window.width / 2 - 250 / 2, window.height / 2 - 50, 250, 50, WIN_STATE, BACK_TO_MAIN_MENU_WIN_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_panel_picture(&panel_list[WIN_STATE].lst_panel_picture[panel_list[WIN_STATE].panel_picture_count++], &animations[PANEL_BROWN_DARK_ANIMATION], 0, 0, window.width, window.height, WIN_STATE);

    // init loose state
    BLV_create_panel(&panel_list[LOOSE_STATE], 1, 1, 1, LOOSE_STATE);
    if (BLV_get_font("comici.ttf", &font, 40, fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        launch = SDL_FALSE;
    }
    launch = launch && BLV_create_label(renderer, &panel_list[LOOSE_STATE].lst_label[panel_list[LOOSE_STATE].label_count++], font, "DEFAITE", 0, 0, window.width, 50, LOOSE_STATE, white, white, white);
    if (BLV_get_font("comici.ttf", &font, 30, fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        launch = SDL_FALSE;
    }
    launch = launch && BLV_create_panel_picture(&panel_list[LOOSE_STATE].lst_panel_picture[panel_list[LOOSE_STATE].panel_picture_count++], &animations[PANEL_BROWN_DARK_ANIMATION], 0, 0, window.width, window.height, LOOSE_STATE);
    launch = launch && BLV_create_button(renderer, &panel_list[LOOSE_STATE].lst_btn[panel_list[LOOSE_STATE].button_count++], font, "Menu principal", window.width / 2 - 250 / 2, window.height / 2 - 50, 250, 50, LOOSE_STATE, BACK_TO_MAIN_MENU_LOOSE_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);

    // init option menu map state
    BLV_create_panel(&panel_list[OPTION_MENU_MAP_STATE], 2, 0, 0, OPTION_MENU_MAP_STATE);
    if (BLV_get_font("comici.ttf", &font, 20, fonts) == SDL_FALSE)
    {
        SDL_Log("Failed to get a font");
        launch = SDL_FALSE;
    }
    launch = launch && BLV_create_button(renderer, &panel_list[OPTION_MENU_MAP_STATE].lst_btn[panel_list[OPTION_MENU_MAP_STATE].button_count++], font, "Quitter", window.width / 2 - 100 / 2, window.height / 2 - 80, 100, 50, OPTION_MENU_MAP_STATE, BACK_TO_MAIN_MENU_OPTION_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[OPTION_MENU_MAP_STATE].lst_btn[panel_list[OPTION_MENU_MAP_STATE].button_count++], font, "Retour", window.width / 2 - 100 / 2, window.height / 2 - 25, 100, 50, OPTION_MENU_MAP_STATE, RETURN_TO_MAP_FROM_OPTION_MENU_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);

    // init see army detail state
    BLV_create_panel(&panel_list[SEE_ARMY_STATE], 1, 6, 7, SEE_ARMY_STATE);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_ARMY_STATE].lst_label[panel_list[SEE_ARMY_STATE].label_count++], font, "Effectif de la cible", window.width / 2 - 200, window.height / 2 - 150, 400, 50, SEE_ARMY_STATE, white, white, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_ARMY_STATE].lst_label[panel_list[SEE_ARMY_STATE].label_count++], font, "0", window.width / 2 - 200 + 80, window.height / 2 - 90, 75, 50, SEE_ARMY_STATE, white, white, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_ARMY_STATE].lst_label[panel_list[SEE_ARMY_STATE].label_count++], font, "0", window.width / 2 - 200 + 205, window.height / 2 - 90, 75, 50, SEE_ARMY_STATE, white, white, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_ARMY_STATE].lst_label[panel_list[SEE_ARMY_STATE].label_count++], font, "0", window.width / 2 - 200 + 330, window.height / 2 - 90, 75, 50, SEE_ARMY_STATE, white, white, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_ARMY_STATE].lst_label[panel_list[SEE_ARMY_STATE].label_count++], font, "0", window.width / 2 - 200 + 130, window.height / 2, 75, 50, SEE_ARMY_STATE, white, white, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_ARMY_STATE].lst_label[panel_list[SEE_ARMY_STATE].label_count++], font, "0", window.width / 2 - 200 + 280, window.height / 2, 75, 50, SEE_ARMY_STATE, white, white, white);
    launch = launch && BLV_create_button(renderer, &panel_list[SEE_ARMY_STATE].lst_btn[panel_list[SEE_ARMY_STATE].button_count++], font, "OK", window.width / 2 + 150, window.height / 2 + 130, 50, 25, SEE_ARMY_STATE, LEAVE_SEE_ARMY_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_ARMY_STATE].lst_panel_picture[panel_list[SEE_ARMY_STATE].panel_picture_count++], &animations[PANEL_BROWN_DARK_ANIMATION], window.width / 2 - 250, window.height / 2 - 190, 500, 380, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_ARMY_STATE].lst_panel_picture[panel_list[SEE_ARMY_STATE].panel_picture_count++], &animations[BANNER_HANGING_ANIMATION], window.width / 2 - 200, window.height / 2 - 150, 400, 50, SEE_ARMY_STATE);

    launch = launch && BLV_create_panel_picture(&panel_list[SEE_ARMY_STATE].lst_panel_picture[panel_list[SEE_ARMY_STATE].panel_picture_count++], &animations[MALE_WARRIOR_ANIMATION], window.width / 2 - 200 + 30, window.height / 2 - 90, 50, 50, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_ARMY_STATE].lst_panel_picture[panel_list[SEE_ARMY_STATE].panel_picture_count++], &animations[MALE_KNIGHT_ANIMATION], window.width / 2 - 200 + 155, window.height / 2 - 90, 50, 50, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_ARMY_STATE].lst_panel_picture[panel_list[SEE_ARMY_STATE].panel_picture_count++], &animations[MALE_THIEF_ANIMATION], window.width / 2 - 200 + 280, window.height / 2 - 90, 50, 50, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_ARMY_STATE].lst_panel_picture[panel_list[SEE_ARMY_STATE].panel_picture_count++], &animations[MALE_ARCHER_ANIMATION], window.width / 2 - 200 + 90, window.height / 2, 50, 50, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_ARMY_STATE].lst_panel_picture[panel_list[SEE_ARMY_STATE].panel_picture_count++], &animations[MALE_WIZARD_ANIMATION], window.width / 2 - 200 + 220, window.height / 2, 50, 50, SEE_ARMY_STATE);

    BLV_create_panel(&panel_list[SEE_CASTLE_STATE], 1, 6, 7, SEE_CASTLE_STATE);
    // init see castel detail state

    launch = launch && BLV_create_label(renderer, &panel_list[SEE_CASTLE_STATE].lst_label[panel_list[SEE_CASTLE_STATE].label_count++], font, "Effectif du chateau", window.width / 2 - 200, window.height / 2 - 150, 400, 50, SEE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_CASTLE_STATE].lst_label[panel_list[SEE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 200 + 80, window.height / 2 - 90, 75, 50, SEE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_CASTLE_STATE].lst_label[panel_list[SEE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 200 + 205, window.height / 2 - 90, 75, 50, SEE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_CASTLE_STATE].lst_label[panel_list[SEE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 200 + 330, window.height / 2 - 90, 75, 50, SEE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_CASTLE_STATE].lst_label[panel_list[SEE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 200 + 130, window.height / 2, 75, 50, SEE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[SEE_CASTLE_STATE].lst_label[panel_list[SEE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 200 + 280, window.height / 2, 75, 50, SEE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_button(renderer, &panel_list[SEE_CASTLE_STATE].lst_btn[panel_list[SEE_CASTLE_STATE].button_count++], font, "OK", window.width / 2 + 150, window.height / 2 + 130, 50, 25, SEE_CASTLE_STATE, LEAVE_SEE_CASTLE_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_CASTLE_STATE].lst_panel_picture[panel_list[SEE_CASTLE_STATE].panel_picture_count++], &animations[PANEL_BROWN_DARK_ANIMATION], window.width / 2 - 250, window.height / 2 - 190, 500, 380, SEE_CASTLE_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_CASTLE_STATE].lst_panel_picture[panel_list[SEE_CASTLE_STATE].panel_picture_count++], &animations[BANNER_HANGING_ANIMATION], window.width / 2 - 200, window.height / 2 - 150, 400, 50, SEE_CASTLE_STATE);

    launch = launch && BLV_create_panel_picture(&panel_list[SEE_CASTLE_STATE].lst_panel_picture[panel_list[SEE_CASTLE_STATE].panel_picture_count++], &animations[MALE_WARRIOR_ANIMATION], window.width / 2 - 200 + 30, window.height / 2 - 90, 50, 50, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_CASTLE_STATE].lst_panel_picture[panel_list[SEE_CASTLE_STATE].panel_picture_count++], &animations[MALE_KNIGHT_ANIMATION], window.width / 2 - 200 + 155, window.height / 2 - 90, 50, 50, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_CASTLE_STATE].lst_panel_picture[panel_list[SEE_CASTLE_STATE].panel_picture_count++], &animations[MALE_THIEF_ANIMATION], window.width / 2 - 200 + 280, window.height / 2 - 90, 50, 50, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_CASTLE_STATE].lst_panel_picture[panel_list[SEE_CASTLE_STATE].panel_picture_count++], &animations[MALE_ARCHER_ANIMATION], window.width / 2 - 200 + 90, window.height / 2, 50, 50, SEE_ARMY_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[SEE_CASTLE_STATE].lst_panel_picture[panel_list[SEE_CASTLE_STATE].panel_picture_count++], &animations[MALE_WIZARD_ANIMATION], window.width / 2 - 200 + 220, window.height / 2, 50, 50, SEE_ARMY_STATE);

    // init battlefield state
    BLV_create_panel(&panel_list[BATTLEFIELD_STATE], 0, 0, 1, BATTLEFIELD_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[BATTLEFIELD_STATE].lst_panel_picture[panel_list[BATTLEFIELD_STATE].panel_picture_count++], &animations[MARBLE3_ANIMATION], 0, 0, window.width, window.height, BATTLEFIELD_STATE);

    // init manage castel state
    BLV_create_panel(&panel_list[MANAGE_CASTLE_STATE], 11, 12, 7, MANAGE_CASTLE_STATE);

    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "CHATEAU", window.width / 2 - 125, window.height / 2 - 150, 100, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 100, window.height / 2 - 100, 50, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 100, window.height / 2 - 50, 50, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 100, window.height / 2, 50, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 100, window.height / 2 + 50, 50, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 - 100, window.height / 2 + 100, 50, 50, MANAGE_CASTLE_STATE, white, black, white);

    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "JOUEUR", window.width / 2 + 25, window.height / 2 - 150, 100, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 + 50, window.height / 2 - 100, 50, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 + 50, window.height / 2 - 50, 50, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 + 50, window.height / 2, 50, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 + 50, window.height / 2 + 50, 50, 50, MANAGE_CASTLE_STATE, white, black, white);
    launch = launch && BLV_create_label(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_label[panel_list[MANAGE_CASTLE_STATE].label_count++], font, "0", window.width / 2 + 50, window.height / 2 + 100, 50, 50, MANAGE_CASTLE_STATE, white, black, white);

    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 - 125, window.height / 2 - 85, 25, 25, MANAGE_CASTLE_STATE, ADD_WARRIOR_TO_CASTEL_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 - 125, window.height / 2 - 35, 25, 25, MANAGE_CASTLE_STATE, ADD_KNIGHT_TO_CASTEL_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 - 125, window.height / 2 + 15, 25, 25, MANAGE_CASTLE_STATE, ADD_ROGUE_TO_CASTEL_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 - 125, window.height / 2 + 65, 25, 25, MANAGE_CASTLE_STATE, ADD_MARKSMAN_TO_CASTEL_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 - 125, window.height / 2 + 115, 25, 25, MANAGE_CASTLE_STATE, ADD_WIZARD_TO_CASTEL_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);

    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 + 100, window.height / 2 - 85, 25, 25, MANAGE_CASTLE_STATE, ADD_WARRIOR_TO_ARMY_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 + 100, window.height / 2 - 30, 25, 25, MANAGE_CASTLE_STATE, ADD_KNIGHT_TO_ARMY_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 + 100, window.height / 2 + 15, 25, 25, MANAGE_CASTLE_STATE, ADD_ROGUE_TO_ARMY_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 + 100, window.height / 2 + 65, 25, 25, MANAGE_CASTLE_STATE, ADD_MARKSMAN_TO_ARMY_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "+", window.width / 2 + 100, window.height / 2 + 115, 25, 25, MANAGE_CASTLE_STATE, ADD_WIZARD_TO_ARMY_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);
    launch = launch && BLV_create_button(renderer, &panel_list[MANAGE_CASTLE_STATE].lst_btn[panel_list[MANAGE_CASTLE_STATE].button_count++], font, "OK", window.width / 2 + 140, window.height / 2 + 120, 50, 25, MANAGE_CASTLE_STATE, LEAVE_MANAGE_CASTEL_BTN, white, black, white, white, blue, blue, &animations[BUTTON_IDDLE_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION], &animations[BUTTON_HOOVER_ANIMATION]);

    launch = launch && BLV_create_panel_picture(&panel_list[MANAGE_CASTLE_STATE].lst_panel_picture[panel_list[MANAGE_CASTLE_STATE].panel_picture_count++], &animations[PANEL_BROWN_DARK_ANIMATION], window.width / 2 - 250, window.height / 2 - 190, 500, 380, MANAGE_CASTLE_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[MANAGE_CASTLE_STATE].lst_panel_picture[panel_list[MANAGE_CASTLE_STATE].panel_picture_count++], &animations[BANNER_HANGING_ANIMATION], window.width / 2 - 200, window.height / 2 - 150, 400, 50, MANAGE_CASTLE_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[MANAGE_CASTLE_STATE].lst_panel_picture[panel_list[MANAGE_CASTLE_STATE].panel_picture_count++], &animations[MALE_WARRIOR_ANIMATION], window.width / 2 - 25, window.height / 2 - 100, 50, 50, MANAGE_CASTLE_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[MANAGE_CASTLE_STATE].lst_panel_picture[panel_list[MANAGE_CASTLE_STATE].panel_picture_count++], &animations[MALE_KNIGHT_ANIMATION], window.width / 2 - 25, window.height / 2 - 50, 50, 50, MANAGE_CASTLE_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[MANAGE_CASTLE_STATE].lst_panel_picture[panel_list[MANAGE_CASTLE_STATE].panel_picture_count++], &animations[MALE_THIEF_ANIMATION], window.width / 2 - 25, window.height / 2, 50, 50, MANAGE_CASTLE_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[MANAGE_CASTLE_STATE].lst_panel_picture[panel_list[MANAGE_CASTLE_STATE].panel_picture_count++], &animations[MALE_ARCHER_ANIMATION], window.width / 2 - 25, window.height / 2 + 50, 50, 50, MANAGE_CASTLE_STATE);
    launch = launch && BLV_create_panel_picture(&panel_list[MANAGE_CASTLE_STATE].lst_panel_picture[panel_list[MANAGE_CASTLE_STATE].panel_picture_count++], &animations[MALE_WIZARD_ANIMATION], window.width / 2 - 25, window.height / 2 + 100, 50, 50, MANAGE_CASTLE_STATE);

    SDL_Log("Creating GUI : %d", launch);
    return launch;
}
/**
 * \brief event file contain all code defition to manage all event, mouse and keyboard include of SDL2
 *
 * \file BLV_event.h
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#ifndef __EVENT_H__
#define __EVENT_H__

#include <SDL.h>

#define MAX_KEY 127

typedef struct 
{
    SDL_bool key[MAX_KEY];
}BLV_Keyboard;

typedef struct 
{
    int x,y;
    SDL_bool left_click_pressed;
    SDL_bool right_click_pressed;
    SDL_bool old_left_click_pressed;
    SDL_bool old_right_click_pressed;
}BLV_Mouse;


/**
 * \brief use to manage all SDL2 event
 * 
 * \param keyboard pointer to keyboard to notify wich key is pressed
 *
 * \return SDL_TRUE if we don't have a quit event or an error, SDL_FALSE instead
 */
SDL_bool BLV_manage_event(BLV_Keyboard *keyboard);

#endif
/**
 * \brief file contain all code implementation for launch and manage a SDL_Renderer and texture with SDL2
 *
 * \file BLV_graphic.c
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#include <SDL_image.h>
#include <dirent.h>

#include "BLV_graphic.h"
#include "BLV_util.h"

SDL_bool BLV_create_BLV_renderer(SDL_Window *w, BLV_Renderer *r)
{
    SDL_Log("Try to create BLV_Renderer");

    if (r == NULL)
    {
        SDL_Log("Failed to create BLV_Renderer, cause r is NULL");
        return SDL_FALSE;
    }

    if (w == NULL)
    {
        memset(r, 0, sizeof(BLV_Renderer));
        SDL_Log("Failed to create BLV_Renderer, cause w is NULL");
        return SDL_FALSE;
    }

    memset(r, 0, sizeof(BLV_Renderer));
    r->idx = -1;
    r->flags = 0;
    r->renderer = SDL_CreateRenderer(w, r->idx, r->flags);

    if (r->renderer == NULL)
    {
        SDL_Log("Failed to create BLV_Renderer, cause %s", SDL_GetError());
        return SDL_FALSE;
    }

    if (SDL_SetRenderDrawBlendMode(r->renderer, SDL_BLENDMODE_BLEND) < 0)
    {
        SDL_Log("Failed to create BLV_Renderer, cause %s", SDL_GetError());
        return SDL_FALSE;
    }

    if (BLV_create_all_texture("./graphics/", r) == SDL_FALSE)
    {
        SDL_Log("Failed to create BLV_Renderer, cause create all texture failed");
        return SDL_FALSE;
    }

    SDL_Log("Create BLV_Renderer success");
    return SDL_TRUE;
}

SDL_bool BLV_create_texture(const char *path, SDL_Renderer *renderer, SDL_Texture **texture)
{
    SDL_Log("Try to create a SDL_Texture");

    if (path == NULL)
    {
        SDL_Log("Failed to create texture, cause path is NULL");
        return SDL_FALSE;
    }

    if (renderer == NULL)
    {
        SDL_Log("Failed to create texture, cause renderer is NULL");
        return SDL_FALSE;
    }

    *texture = IMG_LoadTexture(renderer, path);

    if (*texture == NULL)
    {
        SDL_Log("Failed to create texture, cause %s", SDL_GetError());
        return SDL_FALSE;
    }

    SDL_Log("Create SDL_Texture success with path : %s", path);
    return SDL_TRUE;
}

SDL_bool BLV_create_all_texture(const char *path, BLV_Renderer *r)
{
    SDL_Log("Try to create all texture");

    if (r == NULL)
    {
        SDL_Log("Failed to create all texture, cause r is NULL");
        return SDL_FALSE;
    }

    if (path == NULL)
    {
        SDL_Log("Failed to create all texture, cause path is NULL");
        return SDL_FALSE;
    }

    DIR *open_dir = opendir(path);
    if (open_dir == NULL)
    {
        SDL_Log("Failed to create texture, cause opendir failed with path : %s", path);
        return SDL_FALSE;
    }

    struct dirent *file;

    char buf[100];
    memset(buf, 0, 100);
    BLV_get_formated_date(buf, sizeof(buf));
    SDL_Log("Starting creating all texture at : %s", buf);

    uint64_t start_time = SDL_GetTicks64();
    while ((file = readdir(open_dir)) != NULL)
    {
        if (strcmp(file->d_name, ".") == 0 || strcmp(file->d_name, "..") == 0)
            continue;

        char path_to_texture[255];
        memset(path_to_texture, 0, sizeof(path_to_texture));

        if(strlen(path) + strlen(file->d_name) > 254)
            continue;

        // get path for texture
        memcpy(path_to_texture, path, strlen(path) > (255 - strlen(file->d_name)) ? (255 - strlen(file->d_name)) : strlen(path));
        memcpy(path_to_texture + strlen(path), file->d_name, strlen(file->d_name));

        // fill name for texture
        memcpy(r->texture_array[r->texture_count].name, file->d_name, strlen(file->d_name));

        // creating texture
        if (BLV_create_texture(path_to_texture, r->renderer, &r->texture_array[r->texture_count++].texture) == SDL_FALSE)
        {
            SDL_Log("Failed to create all texture, cause create_texture failed");
            closedir(open_dir);
            open_dir = NULL;
            return SDL_FALSE;
        }
    }
    uint64_t end_time = SDL_GetTicks64();

    memset(buf, 0, 100);
    BLV_get_formated_date(buf, sizeof(buf));
    SDL_Log("Ending creating all texture at : %s, took time : %ld millisecondes", buf, end_time - start_time);

    // close directory
    closedir(open_dir);
    open_dir = NULL;

    SDL_Log("Create all texture success");
    return SDL_TRUE;
}

SDL_bool BLV_clean_BLV_Renderer(BLV_Renderer *r)
{
    SDL_Log("Try to clean BLV_Renderer");

    if (r == NULL)
    {
        SDL_Log("Failed to clean BLV_Renderer, cause r is NULL");
        return SDL_FALSE;
    }

    for (uint16_t i = 0; i < r->texture_count; ++i)
    {
        if (r->texture_array[i].texture == NULL)
            continue;

        SDL_DestroyTexture(r->texture_array[i].texture);
        r->texture_array[i].texture = NULL;
    }

    if (r->renderer != NULL)
    {
        SDL_DestroyRenderer(r->renderer);
        r->renderer = NULL;
    }
    else
        SDL_Log("WARNING : Clean BLV_Renderer ok but SDL_Renderer is NULL");

    memset(r, 0, sizeof(BLV_Renderer));

    SDL_Log("Clean BLV_Renderer success");
    return SDL_TRUE;
}

SDL_bool BLV_get_idx_of_texture(const char *texture_path, uint16_t *idx, BLV_Renderer *r)
{
    if (r == NULL)
    {
        SDL_Log("Failed to clean get idx of texture, cause r is NULL");
        return SDL_FALSE;
    }
    if (texture_path == NULL)
    {
        SDL_Log("Failed to get idx of texture cause texture_path is NULL");
        return SDL_FALSE;
    }
    if (idx == NULL)
    {
        SDL_Log("Failed to get idx of texture cause idx is NULL");
        return SDL_FALSE;
    }

    SDL_bool found = SDL_FALSE;
    for (uint16_t i = 0; i < r->texture_count; ++i)
    {
        if (strcmp(texture_path, r->texture_array[i].name) == 0)
        {
            found = SDL_TRUE;
            *idx = i;
            break;
        }
    }

    return found;
}

SDL_bool BLV_get_texture(const char *texture_path, BLV_Texture **texture, BLV_Renderer *r)
{
    if (r == NULL)
    {
        SDL_Log("Failed to get texture, cause r is NULL");
        return SDL_FALSE;
    }
    if (texture_path == NULL)
    {
        SDL_Log("Failed to get texture, cause texture_path is NULL");
        return SDL_FALSE;
    }

    SDL_bool found = SDL_FALSE;
    for (uint16_t i = 0; i < r->texture_count; ++i)
    {
        if (strcmp(texture_path, r->texture_array[i].name) == 0)
        {
            found = SDL_TRUE;
            *texture = &r->texture_array[i];
            break;
        }
    }

    return found;
}
/**
 * \brief contain all code definition to to all geometry figure
 *
 * \file BLV_geometry.h
 * \author Gautier Levesque
 * \date 03-02-2024
 */
#ifndef __BLV_GEOMETRY_H__
#define __BLV_GEOMETRY_H__

#include <SDL.h>

/**
 * \brief use to define a SDL_Point with a color
 */
typedef struct
{
    SDL_Point p;     /**<p> define SDL_Point of this point*/
    SDL_Color color; /**<color> define color of this point*/
} BLV_Point;

/**
 * \brief use to define a line with a color
 */
typedef struct
{
    int x1;          /**<x1> define first point of a line, x coordinate*/
    int y1;          /**<y1> define first point of a line, y coordinate*/
    int x2;          /**<x2> define second point of a line, x coordinate*/
    int y2;          /**<y2> define second point of a line, y coordinate*/
    SDL_Color color; /**<color> define color of this line*/
} BLV_Line;

/**
 * \brief use to define a SDL_Rect with a color
 */
typedef struct
{
    SDL_Rect r;      /**<r> define SDL_Rect of this rect*/
    SDL_Color color; /**<color> define color of this rect*/
    SDL_bool fill; /**<fill> if we need to fill rect*/
} BLV_Rect;

#endif
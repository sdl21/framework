/**
 * \brief map file contain all code definition for manage map
 *
 * \file BLV_map.h
 * \author Gautier Levesque
 * \date 30-11-2024
 */

#ifndef __BLV_MAP_H__
#define __BLV_MAP_H__

#include "BLV_animation.h"
#include "BLV_geometry.h"
#include "BLV_util.h"

/**
 * \brief use to define size of tile in width
 */
#define TILE_WIDTH 48

/**
 * \brief use to define size of tile in height
 */
#define TILE_HEIGHT 48

/**
 * \brief use to define a tile that is a case of a map
 */
typedef struct
{
    BLV_Animation tile;
    BLV_Rect pos;
    SDL_bool is_blocking;
    SDL_bool can_move;
    void *playerArmy;
    void *npcArmy;
    void *building;
}BLV_Tile;

/**
 * \brief use to define a entire map
 */
typedef struct
{
    BLV_Tile *map;
    uint32_t width;
    uint32_t height;
}BLV_Map;

/**
 * \brief use to init map
 *
 * \param m array of npc that will be fill at the end of this function
 * \param renderer use to create animation and other thing in charge of texture
 * \param width use to define width of map
 * \param height use to define height of map
 *
 * \return SDL_TRUE if map is succefuly create, SDL_FALSE instead
 */
SDL_bool BLV_init_map(BLV_Map *m, BLV_Animation *animations, uint32_t width, uint32_t height);

/**
 * \brief use to update map
 *
 * \param m map
 * \param delta_time time to respect game speed
 * \param state if a battle if occur
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool BLV_update_map(/*BLV_Map *m, uint32_t delta_time, State_of_game state*/);

/**
 * \brief use to clean map
 *
 * \param m map to clean
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool BLV_clean_map(BLV_Map *m);

void BLV_set_map_can_move(int x, int y, int8_t move, BLV_Map *m);
void BLV_reset_map_can_move(BLV_Map *m);

#endif
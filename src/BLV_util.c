/**
 * \brief file contain all code implementation for all utilitary function that can be use everywhere
 *
 * \file BLV_util.c
 * \author Gautier Levesque
 * \date 08-09-2024
 */

#include "BLV_util.h"

void BLV_get_formated_date(char *buffer, size_t buf_size)
{
    time_t rawtime;
    struct tm *timeinfo;
    struct timeval tv;

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    gettimeofday(&tv, NULL);

    snprintf(buffer, buf_size, "%02d-%02d-%04d_%02d:%02d:%02d:%03ld",
             timeinfo->tm_mday,
             timeinfo->tm_mon + 1,
             timeinfo->tm_year + 1900,
             timeinfo->tm_hour,
             timeinfo->tm_min,
             timeinfo->tm_sec,
             tv.tv_usec / 1000);
}

int BLV_manhattan_dist(int x1, int y1, int x2, int y2) 
{
    return abs(x2 - x1) + abs(y2 - y1);
}

double BLV_cartesian_dist(uint32_t pos_x_A, uint32_t pos_y_A, uint32_t pos_x_B, uint32_t pos_y_B)
{
    double dist = 0;

    dist = sqrt((pos_x_A - pos_x_B) * (pos_x_A - pos_x_B) + ((pos_y_A - pos_y_B) * (pos_y_A - pos_y_B)));
    return dist;
}
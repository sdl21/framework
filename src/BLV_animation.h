/**
 * \brief contain all code definition to manage animation of texture
 *
 * \file BLV_animation.h
 * \author Gautier Levesque
 * \date 04-08-2024
 */

#ifndef __BLV_ANIMATION_H__
#define __BLV_ANIMATION_H__

typedef enum
{
    FRONT_WAY = 0,
    BACK_WAY = 4,
    LEFT_WAY = 8,
    RIGHT_WAY = 12,
}BLV_Animation_way_enum;

typedef enum
{
    IDDLE_ANIMATION = 0,
    ATTACK_ANIMATION = 2,
    DIE_ANIMATION = 3,
    WALk_ANIMATION = 1,
}BLV_Animation_type;

typedef enum
{
    BANNER_HANGING_ANIMATION,
    HOUSE01_BLUE_ANIMATION,
    HOUSE01_RED_ANIMATION,
    IMG_FOND_ANIMATION,
    MALE_WARRIOR_ANIMATION,
    MALE_KNIGHT_ANIMATION,
    MALE_THIEF_ANIMATION,
    MALE_ARCHER_ANIMATION,
    MALE_WIZARD_ANIMATION,
    MARBLE3_ANIMATION,
    PANEL_BROWN_DARK_ANIMATION,
    TILEMAP_PACKED_ANIMATION,
    BUTTON_HOOVER_ANIMATION,
    BUTTON_IDDLE_ANIMATION,
    MAX_ANIMATION,
}BLV_Animation_enum;

#include "BLV_graphic.h"

/**
 * \brief use to define how an animation is
 */
typedef struct
{
    BLV_Texture *texture;             /**<texture> current texture of animation*/
    SDL_Rect *src;                    /**<src> array of SDL_Rect represent each frame of animation */
    SDL_RendererFlip flip;            /**<flip> if we need to flip this texture */
    uint8_t currentFramePlayed;       /**<currentFramePlayed> index of src array, frame to print */
    uint8_t maxFrame;                 /**<maxFrame> maximum number of frame for this animation */
    uint16_t animationSpeed;          /**<animationSpeed> each time frame is update */
    uint64_t lastTimeAnimationUpdate; /**<lastTimeAnimationUpdate> previous time since animation is update */
    BLV_Animation_type animationType;
    BLV_Animation_way_enum animationWay;
} BLV_Animation;

/**
 * \brief use to create and init an animation
 * \param animWidth width of one frame animation
 * \param animHeight height of one frame animation
 * \param offset_x offset of place for animation src
 * \param offset_y offset of place for animation src
 * \param maxFrame max frame count to this animation
 *
 * \return SDL_TRUE if success, SDL_FALSE instead (possibly an malloc fail)
 */
SDL_bool BLV_create_BLV_animation(BLV_Animation *animation, uint16_t animationSpeed, uint16_t animWidth, uint16_t animHeight, uint16_t offset_x, uint16_t offset_y, uint8_t maxFrameWidth, uint8_t maxFrameHeight);


SDL_bool BLV_create_array_of_BLV_animation(BLV_Animation *array, BLV_Renderer *renderer);

/**
 * \brief use to update an animation
 *
 * \param animation current animation to update
 */
void BLV_update_BLV_animation(BLV_Animation *animation);

/**
 * \brief use to clean an animation
 *
 * \param animation current animation to clean
 */
SDL_bool BLV_clean_BLV_animation(BLV_Animation *animation);

#endif